import os

CONFIGDB_URL_KEY = os.environ.get('CONFIGDB_URL_KEY') or None
FELIX_SERIAL = os.environ.get('FELIX_SERIAL') or None
FE_SERIALS = os.environ.get('FE_SERIALS') or None
DAQ_CONFIG_TYPE = os.environ.get('DAQ_CONFIG_TYPE') or "daq_config"
CONN_CONFIG_TYPE = os.environ.get('CONN_CONFIG_TYPE') or "conn_config"
SCAN_TREE_NAME = os.environ.get('SCAN_TREE_NAME') or "scan_configs"
SR_URL = os.environ.get('SR_URL') or None
