from .exceptions import UndefinedError

class _DAQ:
    default_values = {
        "chipType": "RD53A",
        "targetCharge": 0,
        "targetTot": 0,
        "targetThresh": 0
    }

    def __init__(self, name = "DAQ"):
        self.name = name

    def get_info(self):
        return {
            "name" : self.name
        }

    def ExecuteScan(self, scan_options):
        raise UndefinedError()
