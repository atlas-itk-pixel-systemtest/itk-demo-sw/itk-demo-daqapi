from ._daq import _DAQ
from .exceptions import FormatError

import logging
import time

"""
Implementation of Dummy DAQ into DAQ API

For testing of API implementation

"""


class DummyDAQ(_DAQ):
    def __init__(self, name = "Dummy DAQ"):
        super().__init__(name)

    def ExecuteScan(self, scan_options):
        try:
            fe_names = [fe["name"] for fe in scan_options['feConfigs']]
        except KeyError as e:
            if e.args[0] == "feConfigs":
                raise FormatError(
                    f"The scan input does not include the required parameter '{e.args[0]}'."
                )
            elif e.args[0] == "name":
                raise FormatError(
                    f"At least one FE does not include the required parameter '{e.args[0]}'."
                )
            else:
                raise FormatError(
                    f"The required parameter '{e.args[0]}' is not given."
                )

        logging.debug("Running scan.")
        n = 65
        for i in range(1, n):
            print(f"Mask Stage {i}")
            time.sleep(0.1)
        time.sleep(5)

        mock_results = {}
        for fe in fe_names:
            mock_results[fe] = {
                "configuration": "Config Data",
                "histogram": "Occupancy Map Data"
            }
        return mock_results
