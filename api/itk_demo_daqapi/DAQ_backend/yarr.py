"""
Implementation of YARR into DAQ API

Requires YARR with python binding to be installed and compiled.
Module 'pyYARR' needs to be accessible via PYTHONPATH.

"""
from ._daq import _DAQ
from .exceptions import FormatError, ResultError, YarrError

import os
import logging
import json
from json.decoder import JSONDecodeError
from pyYARR import ScanConsole


class Yarr(_DAQ):
    def __init__(self, name = "YARR"):
        super().__init__(name)

    @staticmethod
    def _run_counter():
        run_number = 0
        if not os.path.exists(os.getenv("HOME") + "/.yarr"):
            os.makedirs(os.getenv("HOME") + "/.yarr")
        try:
            with open(os.getenv("HOME") + "/.yarr/runCounter", "r") as rc:
                run_number = int(rc.readline())
                rc.close()
        except IOError:
            pass
        run_number = run_number + 1
        with open(os.getenv("HOME") + "/.yarr/runCounter", "w") as rc:
            rc.write(str(run_number))
            rc.close()
        logging.debug(f"yarr run counter {run_number}")
        return run_number

    _DAQ.default_values.update({
        "doPlots": False,
        "logCfgData": "",
        "skipFEconf": False
    })

    default_logging = {
        "pattern": "[%T:%e]%^[%=8l][%=15n]:%$ %v",
        "log_config": [
            {"name": "all", "level": "info"},
            {"name": "Fei4", "level": "trace"},
            {"name": "Fei4Cmd", "level": "trace"},
            {"name": "Rd53a", "level": "trace"},
            {"name": "Rd53aCmd", "level": "trace"},
            {"name": "StarChips", "level": "trace"}
        ]
    }


    def ExecuteScan(self, scan_options):
        # Define necessary variables
        for opt in self.default_values:
            if opt not in scan_options:
                scan_options[opt] = self.default_values[opt]
        con = ScanConsole()

        scanLog = {}
        scanLog["runCounter"] = Yarr._run_counter()

        runConfig = {}
        try:
            # Save given scan options to run-configuration
            if scan_options["targetThresh"] != 0:
                runConfig["targetCharge"] = scan_options["targetThresh"]
            else:
                runConfig["targetCharge"] = scan_options["targetCharge"]

            if scan_options["logCfgData"]:
                runConfig["logConfig"] = scan_options["logCfgData"]
            else:
                runConfig["logConfig"] = self.default_logging

            runConfig["targetTot"] = scan_options["targetTot"]
            runConfig["doPlots"] = scan_options["doPlots"]
            runConfig["skipFEconf"] = scan_options["skipFEconf"]
            runConfig["chipConfig"] = [{
                "chipType": scan_options["chipType"],
                "chips": []
            }]

            # Read controller configuration
            try:
                runConfig["ctrlConfig"] = json.loads(
                    scan_options["ctrlConfigData"])
            except JSONDecodeError:
                raise FormatError(
                    f"Controller configuration cannot be decoded as json."
                )

            # Read scan configuration
            try:
                runConfig["scanCfg"] = json.loads(
                    scan_options["scanConfigData"])
            except JSONDecodeError:
                raise FormatError(
                    f"Scan configuration cannot be decoded as json."
                )

        except KeyError as e:
            raise FormatError(
                f"The scan input does not include the "
                f"required option '{e.args[0]}'."
            )

        try:
            for i, fe in enumerate(scan_options["feConfigs"]):
                chip = {}
                fe_name = fe["name"] if "name" in fe else f"FE_{i+1}"
                chip["__config_path__"] = f"./{fe_name}"
                try:
                    chip["__config_data__"] = json.loads(fe["configData"])
                except JSONDecodeError:
                    raise FormatError(
                        f"The configuration of FE {fe_name} cannot be decoded "
                        f"as json."
                    )

                try:
                    chip["__config_data__"][scan_options["chipType"]]["Parameter"]["Name"] = fe_name
                except KeyError as e:
                    raise FormatError(
                        f"The configuration of FE {fe_name} does not include "
                        f"the required key '{e.args[0]}'."
                    )

                chip["config"] = "./" + fe["name"] + ".json"
                chip["enable"] = fe["enable"]
                chip["locked"] = fe["locked"]
                chip["rx"] = fe["rx"]
                chip["tx"] = fe["tx"]
                runConfig["chipConfig"][0]["chips"].append(chip)

        except KeyError as e:
            if e.args[0] == "feConfigs":
                raise FormatError(
                    f"The scan input does not include the "
                    f"required option '{e.args[0]}'."
                )
            else:
                raise FormatError(
                    f"At least one FE configuration does not include the "
                    f"required option '{e.args[0]}'."
                )

        scanLog["config"] = runConfig

        try:
            print("Loading configs...")
            con.loadConfig(json.dumps(scanLog))
            print("Initialising hardware...")
            con.initHardware()
            print("Configuring...")
            con.configure()
            print("Setting up scan...")
            con.setupScan()
            print("Running scan...")
            con.run()
            print("Getting results...")
            yarr_results = con.getResults()
            print("Cleaning up...")
            con.cleanup()
            del con
        except RuntimeError as e:
            raise YarrError(
                f"An Error occurred during the scan. "
                f"Error message: {e}."
            )

        results = {}
        try:
            allResults = json.loads(yarr_results)
            for fe, result in allResults["frontends"].items():
                results[fe] = {"configuration": " ", "histogram": " "}
                try:
                    results[fe]["configuration"] = json.dumps(result["configs"])
                    results[fe]["histogram"] = json.dumps(result["histos"])
                except KeyError as e:
                    raise ResultError(
                        f"The results for FE '{fe}' from the YARR scan are "
                        f"missing at least one required feature: '{e.args[0]}'."
                    )

        except KeyError as e:
            raise ResultError(
                f"The results obtained from the YARR scan are missing at least "
                f"one required feature: '{e.args[0]}'."
            )
        except AttributeError:
            raise ResultError(
                "Obtained no results from the scan. Please check if you have "
                "enabled at least one FE."
            )
        except JSONDecodeError as e:
            raise ResultError(
                f"The results obtained from the YARR scan are not in the "
                f"required format. Error message: {e}."
            )
        except Exception:
            raise ResultError(
                "An unknown error occured when attempting to obtain the YARR "
                "scan results. Please contact a developer."
            )
        
        return results
