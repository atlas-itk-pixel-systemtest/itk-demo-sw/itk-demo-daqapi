from ._daq import _DAQ
from .exceptions import FormatError, ResultError, FelixDAQError

import json
from json.decoder import JSONDecodeError
from rd53_emulator import (
    DigitalScan,
    AnalogScan,
    ThresholdScan,
    TestScan,
    SensorScan,
    ConfigScan,
    ToTScan,
    PreampTune,
    MaskCheck,
    GlobalThresholdTune,
    PixelThresholdTune
)

"""
Implementation of FELIX DAQ into DAQ API

Requires FELIX DAQ with python binding to be installed and compiled.
The 'rd53_emulator' module needs to be in the PYTHONPATH.

"""


class FelixDAQ(_DAQ):
    def __init__(self, name = "FELIX DAQ"):
        super().__init__(name)

    # Helper function to extract configs for different FEs from the big string they are in
    @staticmethod
    def split_keep(s, delimiter, prefix, suffix):
        split = s.split(delimiter)
        split_delimiter = [prefix + substr +
                           suffix for substr in split[:-1]] + [split[-1]]
        split_delimiter[0] = split_delimiter[0][1:]
        split_delimiter[-1] = prefix+split_delimiter[-1]
        return split_delimiter

    all_scans = {
        "DigitalScan": DigitalScan,
        "AnalogScan": AnalogScan,
        "ThresholdScan": ThresholdScan,
        "TestScan": TestScan,
        "SensorScan": SensorScan,
        "ConfigScan": ConfigScan,
        "TotScan": ToTScan,
        "PreampTune": PreampTune,
        "MaskCheck": MaskCheck,
        "GlobalThresholdTune": GlobalThresholdTune,
        "PixelThresholdTune": PixelThresholdTune
    }

    _DAQ.default_values.update({
        "Backend": "posix",
        "Verbose": False,
        "Interface": "enp24s0f0",
        "Retune": False,
        "CmdLineStr": "Started from Controller"
    })


    def ExecuteScan(self, scan_options):
        # Define necessary variables
        for opt in self.default_values:
            if opt not in scan_options:
                scan_options[opt] = self.default_values[opt]

        try:
            handler = self.all_scans[
                json.loads(scan_options["scanConfigData"])["scan"]["name"]
            ]()
        except KeyError as e:
            raise FormatError(
                f"Scan config does not include the required key '{e.args[0]}'."
            )
        except JSONDecodeError as e:
            raise FormatError(f"Scan config cannot be decoded as json.")

        # Configure Handler
        try:
            handler.SetVerbose(scan_options["Verbose"])
            handler.SetContext(scan_options["Backend"])
            handler.SetInterface(scan_options["Interface"])
            handler.SetRetune(scan_options["Retune"])
            handler.SetCommandLine(scan_options["CmdLineStr"])
            handler.SetCharge(scan_options["targetCharge"])
            handler.SetToT(scan_options["targetTot"])
            handler.SetThreshold(scan_options["targetThresh"])
        except KeyError as e:
            raise FormatError(
                f"The scan input does not include the "
                f"required option '{e.args[0]}'."
            )

        # Mapping config
        try:
            for fe in scan_options["feConfigs"]:
                if fe["enable"]:
                    fe["name"] = (
                        fe["name"]
                        if "name" in fe
                        else f"FE_rx{fe['rx']}_tx{fe['tx']}"
                    )
                    handler.AddMapping(
                        fe["name"],
                        fe["name"] + ".json",
                        fe["tx"],
                        fe["rx"],
                        fe["host"],
                        fe["cmd_port"],
                        fe["host"],
                        fe["data_port"]
                    )
                    handler.AddFEbyString(fe["name"], fe["configData"])
        except KeyError as e:
            if e.args[0] == "feConfigs":
                raise FormatError(
                    f"The scan input does not include the "
                    f"required option '{e.args[0]}'."
                )
            else:
                raise FormatError(
                    f"At least one FE configuration does not include the "
                    f"required option '{e.args[0]}'."
                )

        try:
            handler.Connect()
            handler.InitRun()
            handler.Config()
            handler.PreRun()
            handler.SaveConfig("before")
            handler.Run()
            handler.Analysis()
            allConfigs = handler.GetConfig("after")
            allHistos = handler.GetResults()
            handler.Disconnect()
            del handler
        except RuntimeError as e:
            raise FelixDAQError(
                f"An Error occurred during the scan. "
                f"Error message: {e}."
            )

        results = {}
        try:
            configs = FelixDAQ.split_keep(allConfigs, "}{", "{", "}")
            for i in range(len(configs)):
                if (configs[i].find("FE_")):
                    find_fe_id = configs[i][configs[i].find(
                        "FE_")+3:configs[i].find("FE_")+20]
                    fe_id = "FE_"+find_fe_id.split("\"")[0]
                    if fe_id not in results:
                        results[fe_id] = {
                            "configuration": " ", "histogram": " "}
                    results[fe_id]["configuration"] = configs[i]

            histos = FelixDAQ.split_keep(allHistos, "}{", "{", "}")
            for i in range(len(histos)):
                if (histos[i].find("FE_")):
                    find_fe_id = histos[i][histos[i].find(
                        "FE_")+3:histos[i].find("FE_")+20]
                    fe_id = "FE_"+find_fe_id.split("\"")[0]
                    if fe_id not in results:
                        results[fe_id] = {
                            "configuration": " ", "histogram": " "}
                    results[fe_id]["histogram"] += histos[i]

        except KeyError as e:
            raise ResultError(
                f"The results obtained from the FELIX DAQ scan are missing at "
                f"least one required feature: '{e.args[0]}'."
            )
        except ValueError as e:
            raise ResultError(
                f"Failed to decode the results obtained from the FELIX DAQ "
                f"scan. Error message: {e}."
            )

        return results
