import inspect
from rcf_response.exceptions import RcfException


class DAQException(RcfException):
    def __init__(self, msg, title="DAQ backend Error", status=500, instance=None, ext=None):
        super().__init__(status, title, msg, instance=instance, ext=ext)


class UndefinedError(DAQException):
    def __init__(self):
        fct_name = inspect.stack()[0].function
        msg = f"Function '{fct_name}' has not been defined for this DAQ implementation."
        super().__init__(msg, title="DAQ Function Undefined")


class FormatError(DAQException):
    def __init__(self, msg):
        super().__init__(msg, title="Unsupported input format", status=400)


class ResultError(DAQException):
    def __init__(self, msg):
        super().__init__(msg, title="Scan generated faulty results", status=500)


class FelixDAQError(DAQException):
    def __init__(self, msg):
        super().__init__(msg, title="FELIX DAQ Error", status=500)


class YarrError(DAQException):
    def __init__(self, msg):
        super().__init__(msg, title="YARR Error", status=500)
