from itk_demo_daqapi.routes import daq_router, db_router
from rcf_response import Error
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import RedirectResponse


def create_app(config=None):
    app = FastAPI(
        title="DAQ API",
        description="HTTP API to the DAQ backend",
        version="1.1.0",
        docs_url="/api/docs",  # custom location for swagger UI
    )
    app.include_router(daq_router)
    app.include_router(db_router)

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],  # Allows all origins
        allow_credentials=True,
        allow_methods=["*"],  # Allows all methods
        allow_headers=["*"],  # Allows all headers
    )

    @app.get('/', include_in_schema=False)
    async def index():
        return RedirectResponse("/api/docs")

    # # TODO: Adapt to FastAPI syntax! Also overwrite validation errors.
    # with app.app.app_context():
    #     app.add_error_handler(404, flask_error_response)
    #     app.add_error_handler(405, flask_error_response)
    #     app.add_error_handler(500, flask_error_response)

    return app


# def create_flask_app(config=None):
#     app = create_app(config)
#     return app.app


# def flask_error_response(error):
#     error = Error(error.code, error.name, error.description + f" ({request.url}).")
#     return error.to_conn_response()
