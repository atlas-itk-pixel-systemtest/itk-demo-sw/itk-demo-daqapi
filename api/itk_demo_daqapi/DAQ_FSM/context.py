from .none import NoneState
from .idle import Idle
from .running import Running
from .done import Done
from .error import ErrorState
from .exceptions import DbmError, InputError, StateError, InternalError
from ..DAQ_API_Config import SR_URL

import dbm
from pyconfigdb.configdb import ConfigDB
from pyconfigdb.exceptions import DBException
from pyregistry import ServiceRegistry
from pyregistry.exceptions import SRException


class Context:
    def __init__(
        self,
        available_daqs : list,
        felix_serial : str,
        fe_serials : list[str],
        db_key : str,
        daq_config_type : str,
        conn_config_type : str,
        scan_tree_name : str
    ):
        # Variables to be shared among workers
        self.dbm_name = "data_store"
        self.states = {
            "NoneState": NoneState,
            "Idle": Idle,
            "Running": Running,
            "Done": Done,
            "Error": ErrorState
        }

        # Variables that are set at start-up: No need for worker-sharing
        self.available_daqs = available_daqs if isinstance(
            available_daqs, dict) else {}

        # Serial numbers of FELIX and all FEs assigned to the DAQ API
        self.felix_serial = felix_serial
        self.fe_serials = fe_serials
        
        # Configrable config names
        self.daq_config_type = daq_config_type
        self.conn_config_type = conn_config_type
        self.scan_tree_name = scan_tree_name
        
        # Connect to database
        try:
            self.db = ConfigDB(db_key, SR_URL)
        except DBException as e:
            raise InputError(
                f"Failed to connect to Config DB with key '{db_key}'."
            ) from e

        # Connect to Service Registry
        try:
            self.sr = ServiceRegistry(srUrl=SR_URL)
        except SRException as e:
            self.sr = None
        
        # Register default state and daq
        self.set_state("NoneState")
        self.set_daq("")

    def _get_dbm_value(self, key):
        while True:
            try:
                with dbm.open(self.dbm_name, 'c') as db:
                    try:
                        return str(db[key], encoding = 'utf-8')
                    except KeyError as e: 
                        raise DbmError(
                            f"The key '{key}' does not exist in the data store "
                            f"'{self.dbm_name}'."
                        )
            except dbm.error:
                pass
        
    def _set_dbm_value(self, key, value):
        while True:
            try:
                with dbm.open(self.dbm_name, 'c') as db:
                    try:
                        db[key] = value.encode('utf-8')
                        return
                    except KeyError as e: 
                        raise DbmError(
                            f"The key '{key}' does not exist in the data store "
                            f"'{self.dbm_name}'."
                        )
            except dbm.error:
                pass


    def set_state(self, stateName):
        if stateName not in self.states:
            raise StateError(
                f"State '{stateName}' not in list of available states."
            )

        self._set_dbm_value("state", stateName)
        
    def get_state(self):
        stateName = self._get_dbm_value("state")
        try:
            return self.states[stateName]()
        except KeyError:
            raise StateError(
                f"State '{stateName}' not in list of available states."
            )

    def set_daq(self, daqName):
        if daqName not in self.available_daqs and not daqName == "":
            raise StateError(
                f"DAQ '{daqName}' not in list of available states."
            )

        self._set_dbm_value("daq", daqName)

    def get_daq(self):
        daqName = self._get_dbm_value("daq")
        if daqName == "":
            return None
        try:
            return self.available_daqs[daqName]()
        except KeyError:
            raise InternalError(
                f"DAQ '{daqName}' not in list of available states."
            )

    def get_available_daqs(self):
        return list(self.available_daqs.keys())
