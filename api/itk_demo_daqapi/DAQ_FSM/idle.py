from .state import State
from .exceptions import InputError, InternalError, ResultError

import traceback
import json
from json.decoder import JSONDecodeError
from rcf_response import Info, Error
from rcf_response.exceptions import RcfException
from pyconfigdb.exceptions import DBException
from ..request_bodies import ScanBody
import datetime

class Idle(State):
    def __init__(self):
        self.name = "Idle"

    @staticmethod
    def _get_payload_of_type(context, payloads: list[dict], type_str: str):
        """
        Helper method to get the object payload of a specific type.
        
        Parameters
        ----------
        context : Context
            Context object of the FSM. Contains Config DB object.
        payloads : list[dict]
            List of payload information dicts, not containing the payload data. 
        type_str : str
            Type of payload to search for. First match will be returned.
        
        Returns
        -------
        data : str
            Config payload data
        """
        
        for p in payloads:
            if p["type"] == type_str:
                p_res = context.db.payload_read(p["id"], stage = False)
                return p_res["data"]

        raise InputError(f"Given payloads to not contain type '{type_str}'.")

    @staticmethod
    def _get_object_payload_of_type(context, id: str, type_str: str = "config"):
        """
        Helper method to get a single config payload based on either an object 
        uuid or a tag. 
        
        Parameters
        ----------
        context : Context
            Context object of the FSM. Contains Config DB object.
        id : str | None
            uuid or tag of the config object. First searches for matching tag, then falls back to uuid.
            
        Returns
        -------
        data : str
            Config payload data
        """
        try:
            res_tree = context.db.tag_tree(id, depth = 1, stage = False)
        except DBException:
            try:
                res_tree = context.db.object_tree(id, depth = 1, stage = False)
            except DBException as ee:
                raise InputError(
                    f"The given id does not match any tag or uuid in the Config DB."
                    f"Given id: '{id}'"
                ) from ee 

        try: 
            return Idle._get_payload_of_type(context, res_tree["payloads"], type_str)
        except InputError:
            raise InputError(
                f"Given object does not have an associated payload of type {type_str}."
                f"given id: '{id}'"
            )

    @staticmethod
    def _get_runkey_tree(context, id: str):
        """
        Helper method to get a full runkey tree based on either an object 
        uuid or a tag. 
        
        Parameters
        ----------
        context : Context
            Context object of the FSM. Contains Config DB object.
        id : str | None
            uuid or tag of the config object. First searches for matching tag, then falls back to uuid.
            
        Returns
        -------
        tree : dict
            Runkey tree as dict
        """
        try:
            res = context.db.tag_tree(id, stage = False)["objects"][0]
        except DBException as e:
            try:
                res = context.db.object_tree(id, stage = False)
            except DBException as ee:
                raise InputError(
                    f"The given id does not match any tag or uuid in the Config DB."
                    f"Given id: '{id}'"
                ) from ee 

        return res

    @staticmethod
    def _get_scan_config(context, scan_type):
        """
        Helper method to obtain a scan config from the Config DB, by selecting 
        the matching object from the scan_config tree.
        
        Parameters
        ----------
        context : Context
            Context object of the FSM. Contains Config DB object.
        scan_type : str
            Requested scan type. 
            
        Returns
        -------
        config : str
            Scan config data.
        
        """
        scan_configs = context.db.search_subtree(
            context.scan_tree_name,
            object_type = scan_type,
            payload_data = True
        )
        if len(scan_configs) == 0:
            raise InputError(
                f"The scan tree {context.scan_tree_name} does not contain a "
                f"scan of type {scan_type}."
            )
    
        for p in scan_configs[0]["payloads"]:
            if p["type"] == "config":
                return p["data"]
        
        raise InputError(
            f"The given scan of type {scan_type} in the scan tree "
            f"{context.scan_tree_name} does not contain a payload of type 'config'."
        )
        
    @staticmethod
    def _get_relevant_runkey_section(context, runkey: str):
        """
        Helper method to select the relevant objects from a full runkey tree. 
        Selection is based on the felix and frontend serial numbers given at startup. 
        Allows to only get payloads for the relevant parts of the system.

        Parameters
        ----------
        context : Context
            Context object of the FSM. Contains Config DB object.
        runkey : str
            Runkey identifier. Can be either uuid or tag.
            
        Returns
        ----------
        felix : dict
            Information on FELIX and related payloads. Only payload metadata is 
            included, actual payload data needs to be requested separately. 
            Structure of dict: 
            {
                "obj_uuid" : str,
                "serial" : str,
                "payloads" : list[dict]
            }
        fe_list : list[dict]
            List of fes relevant for the scan. Entries are structured 
            identically to felix-dict.
        """
        search_trees = context.db.search_subtree(
            runkey,
            object_type = "felixCard",
            search_dict = {"serial" : context.felix_serial}
        )
        if len(search_trees) == 0:
            raise InputError(
                f"The given runkey does not contain a FELIX card with the "
                f"serial number {context.felix_serial}."
            )
        search_tree = search_trees[0]
        felix = {
            "obj_uuid" : search_tree["id"],
            "serial" : search_tree["metadata"]["serial"],
            "payloads" : search_tree["payloads"]
        }
        
        
        fe_list = []
        def recursive_serial_search(node):
            """
            Internal helper function to obtain all relevant FEs from the runkey. 
            """
            try: 
                if (
                    node["type"] == "frontend" 
                    and node["metadata"]["serial"] in context.fe_serials
                ):
                    fe_list.append({
                        "obj_uuid" : node["id"],
                        "serial" : node["metadata"]["serial"],
                        "payloads" : node["payloads"]
                    })
                    return 
            except KeyError:
                pass
            
            if "children" in node:
                for c in node["children"]:
                    recursive_serial_search(c)
            else:
                return
        
        recursive_serial_search(search_tree)
        return felix, fe_list

    @staticmethod
    def _prepare_results(
        context, 
        felix_serial: str, 
        fe_serials: list[str], 
        scan_cfg_id: str
    ):
        """
        Helper method to create an empty results entry in the Config DB staging
        area. The entry is to be filled after a successful scan.
        
        Parameters
        ----------
        context : Context
            Context object of the FSM. Contains Config DB object.
        runkey : str
            Tag or ID of related runkey.
        felix_serial : str
            Serial number of the connected FELIX. Added to the entry as metadata.
        fe_serials : list[str]
            Serial numbers of the connected FEs. Added to the entry as metadata.
        scan_cfg_id: str
            Scan config identifier (type of child). Added to the entry as metadata.
            
        Returns
        ----------
        result_id : str
            uuid of the created object in the staging area.
        """
        
        result_id = context.db.add_node(
            "scan_result",
            payloads = [{
                "meta" : True,
                "type" : "scan_meta",
                "data" : json.dumps({
                    "felix_serial" : felix_serial,
                    "fe_serials" : fe_serials,
                    "scan_type" : scan_cfg_id,
                    "start_time" : '{:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now())
                })
            }],
            # parents = [{"id" : runkey_id, "view" : 1}]
        )
        
        return result_id

    @staticmethod
    def _save_results(context, runkey: str, result_id: str, results: dict):
        """
        Helper method to add scan results to predefined results node. 
        
        Parameters
        ----------
        context : Context
            Context object of the FSM. Contains Config DB object.
        runkey : str
            uuid or tag of the scan runkey.
        result_id : str
            uuid of the predefined results object.
        results : dict
            Results as obtained from the DAQ backend. Nested dictionary of the 
            following structure: 
            {
                <FE_serial> : {
                    "configuration" : str,
                    "histogram" : str
                }, ...
            }
        
        """
        if type(results) not in [json, dict]:
            raise ResultError(
                "Scan results are not in the required json format. "
                "Cannot save to DB."
            )
        
        try:
            runkey_id = context.db.tag_tree(runkey, depth = 1, stage = False)["objects"][0]["id"]
        except DBException:
            runkey_id = runkey
        
        for fe_serial, fe_results in results.items():
            payloads = [
                {
                    "type" : result_type,
                    "data" : data,
                    "meta" : False
                } for result_type, data in fe_results.items()
            ]
            payloads.append({
                    "meta" : True,
                    "type" : "fe_metadata",
                    "data" : json.dumps({
                        "serial" : fe_serial
                    })
                }
            )
            
            
            context.db.add_node( 
                "frontend", payloads = payloads, parents = [{"id" : result_id, "view" : 1}]
            )
        
        committed_result_id = context.db.stage_commit(result_id)[0]
        print("Comitted Result ID: ", committed_result_id)
        print("Runkey ID: ", runkey_id)
        res = context.db.add_to_node(runkey_id, children = [{"id" : committed_result_id, "view" : 1}], stage = False)

        return committed_result_id

    @staticmethod
    def _prepare_scan_input(
       context, scan_cfg_id: str, felix: dict, fes: list[dict]
    ):
        """
        Helper method to get all config payload data from the Config DB and 
        organise everything into the input format required by the DAQ backend.
        
        Parameters
        ----------
        context : Context
            Context object of the FSM. Contains Config DB object.
        scan_cfg_id : str
            ID or tag of the scan configuration.
        felix : dict
            FELIX information dict, containing its uuid, serial number and 
            payload information (without the payload data). Can be obtained with 
            the _get_relevant_runkey_section(...) method.
        fes: list[dict]
            List of FE information dicts, containing its uuid, serial number and 
            payload information (without the payload data). Can be obtained with 
            the _get_relevant_runkey_section(...) method.
            
        Returns
        ----------
        scan_params : dict
            Scan options in the format accepted by the DAQ backend. 

        """
        
        scan_params = { }
        
        # Get scan config data
        scan_params["scanConfigData"] = Idle._get_scan_config(context, scan_cfg_id)
        
        # Get FELIX config data
        try:
            scan_params["ctrlConfigData"] = Idle._get_payload_of_type(context, felix["payloads"], context.daq_config_type)
        except DBException:
            raise InputError(
                f"Cannot find scan config payload of type 'config' for id {scan_cfg_id}"
            )
            
        # Get FE config and connectivity data
        scan_params["feConfigs"] = []
        for fe in fes: 
            try: 
                config_data = Idle._get_payload_of_type(context, fe["payloads"], context.daq_config_type)
            except InputError: 
                raise InputError(
                    f"FE with serial number {fe['serial']} does not contain "
                    f"payload of type 'config'."
                )
            try:
                conn_data = Idle._get_payload_of_type(context, fe["payloads"], context.conn_config_type)
            except InputError: 
                raise InputError(
                    f"FE with serial number {fe['serial']} does not contain "
                    f"payload of type 'connectivity'."
                )
            
            try: 
                conn_dict = json.loads(conn_data)
            except JSONDecodeError:
                raise InputError(
                    f"Connectivity config of FE with serial number "
                    f"{fe['serial']} is not a valid json."
                )
                
            try:
                scan_params["feConfigs"].append({
                    "name": fe["serial"],
                    "configData": config_data,
                    "tx": conn_dict["tx"],
                    "rx": conn_dict["rx"],
                    "enable": conn_dict["enable"],
                    "locked": conn_dict["locked"]
                })
            except KeyError as e:
                raise InputError(
                    f"Connectivity config of FE with serial number {fe['serial']} "
                    f"does not include the required attribute '{e.args[0]}'."
                )
            for optional_opt in ["host", "cmd_port", "data_port"]:
                try:
                    scan_params["feConfigs"][-1][optional_opt] = conn_dict[optional_opt]
                except: 
                    pass
            return scan_params


    # DAQ endpoints

    def on_init(self, context, params):
        return Error(
            400,
            "Already initialized",
            "The DAQ API has already been initialized. Restart the service if "
            "you wish to reconfigure it."
        )

    def on_run(self, context, params: ScanBody):
        context.set_state("Running")
        
        try:
            # get relevant section of runkey from db
            felix, fes = Idle._get_relevant_runkey_section(context, params.runkey)
            
            # Obtain payload data (for FE, FELIX, and scan configs) and transform to 
            # DAQ-backend compatible format
            scan_params = Idle._prepare_scan_input(context, params.scan_config, felix, fes)
            
            # Add additional information
            scan_params["targetThresh"] = params.targetThreshold
            scan_params["targetTot"] = params.targetTOT
            scan_params["targetCharge"] = params.targetCharge
            
            # Create empty tree to store results (in staging area)
            #   Should contain: 
            #     - felix-serial, fe-serials as metadata
            #     - original runkey tag
            #     - fe results will be added as children
            result_id = params.output_tree
            if result_id is None:
                result_id = Idle._prepare_results(
                    context, 
                    felix["serial"],
                    [fe["serial"] for fe in fes],
                    params.scan_config
                )
            
            # Execute scan
            results = context.get_daq().ExecuteScan(scan_params)
            
            # Save results to results tree and commit staged results tree
            committed_result_id = Idle._save_results(context, params.runkey, result_id, results)
            
            context.set_state("Done")
            return Info(
                f"Successfully executed scan with ID {committed_result_id}.",
                payload={"result_id": committed_result_id}
            )
        except RcfException as e:
            context.set_state("Idle")
            return Error.from_exception(e)
        except Exception as e:
            context.set_state("Error")
            return Error(
                500,
                "Scan Error",
                "An unknown error occurred during scan execution. Please "
                "contact a developer.",
                _ext = {
                    "Exception" : str(e),
                    "Traceback" : traceback.format_exc()
                }
            )
