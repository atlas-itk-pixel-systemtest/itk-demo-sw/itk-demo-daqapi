from .idle import Idle
from rcf_response import Error
from ..request_bodies import ScanBody

class Running(Idle):
    def __init__(self):
        self.name = "Running State"

    @staticmethod
    def _busyError():
        return Error(
            400,
            "DAQ API Busy",
            "The DAQ API is busy. Your request cannot be handled at the moment."
        )

    # DAQ Endpoints

    def on_run(self, context, params: ScanBody):
        return Running._busyError()

