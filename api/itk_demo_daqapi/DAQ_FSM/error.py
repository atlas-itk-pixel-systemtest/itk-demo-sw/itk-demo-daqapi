from .none import NoneState

from rcf_response import Error


class ErrorState(NoneState):
    def _errorStateError(self, endpoint=""):
        if endpoint == "":
            error_text = (
                "The DAQ API is in Error state. "
                "This endpoint is not available at the moment. "
                "Please re-initialize the service."
            )
        else:
            error_text = (
                f"The DAQ API is in Error state. "
                f"The {endpoint} endpoint is not available at the moment. "
                f"Please re-initialize the service."
            )
        return Error(400, "DAQ API in Error state", error_text)

    def __init__(self):
        self.name = "Error State"

    # DAQ Endpoints
    def on_run(self, context, params):
        return self._errorStateError("/daq/run")
