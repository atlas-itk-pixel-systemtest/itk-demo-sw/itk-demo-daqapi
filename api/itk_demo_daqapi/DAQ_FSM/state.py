import inspect
from rcf_response import Error, Info
from rcf_response.exceptions import RcfException
from pyconfigdb.exceptions import DBException
from ..request_bodies import InitBody, ScanBody
import traceback
from .exceptions import InputError


class State:
    def __init__(self):
        self.name = ""

    def __repr__(self):
        return f"<State object - {self.name}>"

    def _undefinedError(self, endpoint: str):
        error_text = (
            f"The '{endpoint}' endpoint has not been implemented "
            f"for the current state '{self.name}'."
        )
        return Error(500, "State Error", error_text)

    # DAQ endpoints

    def on_health(self, context):
        output = Info()

        return output

    def on_info(self, context):
        output = Info()
        output.payload["status"] = 200
        output.payload["state"] = context.get_state().name
        output.payload["felix"] = context.felix_serial
        output.payload["fe_serials"] = context.fe_serials

        try:
            output.payload["daqInfo"] = context.get_daq().get_info()
        except AttributeError:
            output.payload["daqInfo"] = {
                "name" : ""
            }
        return output

    def on_init(self, context, params: InitBody):
        return self._undefinedError("/daq/init")

    def on_available_daqs(self, context):
        try: 
            daqs = context.get_available_daqs()
            return Info(payload={"daqs": daqs})
        except RcfException as e: 
            return Error.from_exception(e)
        except Exception as e:
            context.set_state("Error")
            return Error(
                500,
                "Internal Server Error",
                "An unknown error occurred during scan execution. Please "
                "contact a developer.",
                _ext = {
                    "Exception" : str(e),
                    "Traceback" : traceback.format_exc()
                }
            )

    def on_run(self, context, params: ScanBody):
        return self._undefinedError("/daq/run")

    # DB endpoints

    def on_get_runkeys(self, context):
        try:
            rk_list = context.db.read_all("tag", filter = "runkey", stage=False)
            output = Info(payload = {"runkeys" : [rk["name"] for rk in rk_list]})
            return output 
        except RcfException as e: 
            return Error.from_exception(e)

    def on_get_scan_configs(self, context):
        try:
            try:
                res = context.db.tag_tree(context.scan_tree_name, stage = False)["objects"][0]
            except DBException as e:
                try:
                    res = context.db.object_tree(context.scan_tree_name, stage = False)
                except DBException as ee:
                    raise InputError(
                        f"The given scan tree id {context.scan_tree_name} does not "
                        f"match any tag or uuid in the Config DB."
                    )
            try:
                configs = [c["type"] for c in res["children"]]
                output = Info(payload = {"scanConfigs" : configs})
                return output 
            except KeyError as e: 
                raise InputError(
                    f"The given scan tree with id {context.scan_tree_name} is "
                    f"not in the required format."
                ) from e
        except RcfException as e:
            return Error.from_exception(e)
        except Exception as e:
            return Error(
                500,
                "Internal Server Error",
                "An unknown error occurred during scan execution. Please "
                "contact a developer.",
                _ext = {
                    "Exception" : str(e),
                    "Traceback" : traceback.format_exc()
                }
            )
        
