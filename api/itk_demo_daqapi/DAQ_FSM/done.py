from .idle import Idle


class Done(Idle):
    def __init__(self):
        self.name = "Done State"
