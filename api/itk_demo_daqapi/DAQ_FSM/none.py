from .state import State

from rcf_response import Info, Error
from rcf_response.exceptions import RcfException
from ..request_bodies import InitBody, ScanBody


class NoneState(State):
    def __init__(self):
        self.name = "None State"

    @staticmethod
    def _uninitializedError(endpoint):
        error_text = (
            f"The DAQ API has not been initialized yet. The {endpoint} "
            f"endpoint is available after initialization."
        )
        return Error(400, "DAQ API Uninitialized", error_text)

    # DAQ endpoints

    def on_init(self, context, params: InitBody):
        try:
            context.set_daq(params.daq_system)
            context.set_state("Idle")
            return Info(
                f"Successfully initialized DAQ API with DAQ '{params.daq_system}'."
            )
        except RcfException as e:
            return Error.from_exception(e)

    def on_run(self, context, params: ScanBody):
        return NoneState._uninitializedError("/scan/manual")
