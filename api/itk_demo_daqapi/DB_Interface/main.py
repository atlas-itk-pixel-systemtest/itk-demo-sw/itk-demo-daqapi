from itk_demo_daqapi.DB_Interface.config_db import ConfigDB

if __name__ == "__main__":
    dbkey = "demi/default/itk-demo-configdb/api/url"
    headers = {"content-type": "application/json", "accept": "application/json"}
    db = ConfigDB(dbkey, headers)
    runkeys = db.get_runkey_list()

    print(runkeys)
