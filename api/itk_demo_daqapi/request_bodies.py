from pydantic import BaseModel 
from typing import Union

class InitBody(BaseModel):
    daq_system: str
    
    model_config = {
        "json_schema_extra": {
            "examples": [
                {"daq_system": "YARR"}
            ]
        }
    }
    
class ScanBody(BaseModel):
    scan_config: Union[str, int] # ID or tag of scan config
    runkey: Union[str, int] # ID or tag of runkey
    output_tree: Union[str, int] = None
    targetTOT: int = 1000
    targetThreshold: int = 1000
    targetCharge: int = 1000
    
    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "scan_config": "my_scan_cfg_tag",
                    "runkey" : "my_runkey_tag",
                    "output_tree" : "custom_output_tree_id"
                }
            ]
        }
    }
    