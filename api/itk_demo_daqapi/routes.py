from .DAQ_API_Config import (
    CONFIGDB_URL_KEY,
    FELIX_SERIAL,
    FE_SERIALS,
    DAQ_CONFIG_TYPE,
    CONN_CONFIG_TYPE,
    SCAN_TREE_NAME
)
from .DAQ_FSM import Context

import importlib
from fastapi import APIRouter, Query, Request, HTTPException, Response
from fastapi.responses import HTMLResponse, StreamingResponse
import logging
from .request_bodies import InitBody, ScanBody
logging.basicConfig(level=logging.INFO)

#TODO: Add configuration for hardware serial numbers

###########################
### Prepare DAQ Systems ###
###########################
from .DAQ_backend import DummyDAQ
available_daqs = {"Dummy DAQ": DummyDAQ}
if importlib.util.find_spec("pyYARR") is not None:
    from itk_demo_daqapi.DAQ_backend import Yarr
    available_daqs.update({"YARR": Yarr})
if importlib.util.find_spec("rd53_emulator") is not None:
    from itk_demo_daqapi.DAQ_backend import FelixDAQ
    available_daqs.update({"FELIX DAQ": FelixDAQ})

##########################################
### Check and transform serial numbers ###
##########################################
if FELIX_SERIAL is None:
    raise Exception(f"FELIX_SERIAL is not specified. Necessary for running.")
if FE_SERIALS is None:
    raise Exception("FE_SERIALS is not specified. Necessary for running.")
if CONFIGDB_URL_KEY is None:
    raise Exception("CONFIGDB_URL_KEY is not specified. Necessary for running.")

#############################
### Create Context object ###
#############################
context = Context(
    available_daqs,
    FELIX_SERIAL,
    FE_SERIALS.split(","),
    CONFIGDB_URL_KEY,
    DAQ_CONFIG_TYPE,
    CONN_CONFIG_TYPE,
    SCAN_TREE_NAME
)


###################################
### DAQ Endpoints and Functions ###
###################################
daq_router = APIRouter(prefix="/api/daq", tags=["DAQ"])

@daq_router.get(
    "/health",
    summary="Get health information",
    description="Obtain health information on the DAQ Interface.",
)
async def health():
    global context
    return Response(status_code=200)# return 200 # context.get_state().on_health(context).to_conn_response()[0]

@daq_router.get(
    "/info",
    summary="Get DAQ API information",
    description="Obtain information on the state, the connected DAQ and the connectivity of the DAQ Interface."
)
async def get_info():
    return context.get_state().on_info(context).to_conn_response()[0]

@daq_router.post(
    "/init",
    summary="Initialise the DAQ API",
    description="Initialise the DAQ API with an available DAQ system."
)
def init(params: InitBody):
    # Select DAQ
    global context
    return context.get_state().on_init(context, params).to_conn_response()[0]

@daq_router.get(
    "/available_daqs",
    summary="Get available DAQ systems",
    description="Obtain a list of all currently available DAQ systems."
)
async def available_daqs():
    global context
    return context.get_state().on_available_daqs(context).to_conn_response()[0]

@daq_router.post(
    "/run",
    summary="Perform a scan",
    description="Perform a scan or tune with the initialised DAQ System."
)
def run(params: ScanBody):
    global context
    return context.get_state().on_run(context, params).to_conn_response()[0]


#########################################
### Config DB Endpoints and Functions ###
#########################################
db_router = APIRouter(prefix="/api/db", tags=["Config DB"])

@db_router.get(
    "/runkeys",
    summary="Get available runkeys",
    description="Obtain a list of available runkey tags in the Config DB."
)
async def get_runkeys():
    # Get list of runkey tags from the DB
    global context
    return context.get_state().on_get_runkeys(context).to_conn_response()[0]

@db_router.get(
    "/scan_configs",
    summary="Get available scan configurations",
    description="Obtain a list of available scan configurations in the Config DB."
)
async def get_scan_configs():
    # get list of scan configs from the DB
    global context
    return context.get_state().on_get_scan_configs(context).to_conn_response()[0]

