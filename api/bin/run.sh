#!/usr/bin/env bash
#GUNICORN
CUR_DIR=`pwd`
SCRIPT=`realpath -- $BASH_SOURCE`
SCRIPTPATH=`dirname $SCRIPT`
cd "$SCRIPTPATH/.."
poetry run gunicorn wsgi:app --bind 0.0.0.0:5005 --workers=4 --timeout 600
cd $CUR_DIR
