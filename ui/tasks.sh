#!/usr/bin/env bash
#
# ITk demonstrator taskfile
#
# 2022, Gerhard Brandt <gbrandt@cern.ch>
#

script_dir="$(dirname "$0")"
alias run=./tasks.sh

export GITLAB_PROJECTID=120785
export GITLAB_PROJECT=itk-demo-daqapi

export GITLAB_SERVER=gitlab.cern.ch
export GITLAB_ROOTGROUP=atlas-itk-pixel-systemtest
export GITLAB_SUBGROUP=itk-demo-sw
# export GITLAB_FEATURE=feature/my-feature-0

export GITLAB_REGISTRY=gitlab-registry.cern.ch
export GITLAB_REGISTRY_IMAGE=${GITLAB_REGISTRY}/${GITLAB_ROOTGROUP}/${GITLAB_SUBGROUP}/${GITLAB_PROJECT}/${GITLAB_PROJECT}-ui

echo "image: ${GITLAB_REGISTRY_IMAGE}"

# GITLAB_TOKEN=$(cat ./.gitlab-token)
# export GITLAB_TOKEN

export NAMESPACE="demi.${GITLAB_PROJECT}"
PORT=5009
PUID=$(id -u)
PGID=$(id -g)
PPWD=$(pwd)
export PUID PGID PPWD PORT

function obtian_settings {
  export USE_REGISTRY_CACHE=${USE_REGISTRY_CACHE:-1}
  export ETCD_HOST=${ETCD_HOST:-$(hostname)}
  export ETCD_PORT=${ETCD_PORT:-2379}
  export API_HOST="${API_HOST:-$(hostname)}"
  export API_PORT="${API_PORT:-5005}"
  export API_WORKERS=4
  export UI_PORT="${UI_PORT:-5085}"
}

function read_cache {
  source ${script_dir}/../.registry_cache 2>/dev/null
}
# Local tasks

function install {
  NVM_VERSION=16.13.2

  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
  export NVM_DIR="$HOME/.nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
  [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

  nvm install ${NVM_VERSION}
  nvm use ${NVM_VERSION}

  npm install
  npm run build
}

# Docker tasks

function all {
  # build
  # up  
  help
}

function build {
  echo "Building Docker image"
  docker build -t ${GITLAB_REGISTRY_IMAGE}:latest .
}

function rebuild {
  docker build -t ${GITLAB_REGISTRY_IMAGE}:latest . --no-cache
}

function push {
  docker login ${GITLAB_REGISTRY}
  docker push ${GITLAB_REGISTRY_IMAGE}:latest
}

function pull {
  docker login ${GITLAB_REGISTRY}
  docker pull ${GITLAB_REGISTRY_IMAGE}:latest
  # docker tag ${GITLAB_REGISTRY_IMAGE}:latest ${GITLAB_PROJECT}
}

function run {
  docker run --rm -it ${GITLAB_REGISTRY_IMAGE}:latest bash
  # docker run --rm -it ${GITLAB_PROJECT} bash
}

function exec {
  docker exec -it ${GITLAB_REGISTRY_IMAGE}:latest ${1-bash}
  docker exec -it ${GITLAB_PROJECT} ${1-bash}
}

function ps {
  docker compose ps
}

function logs {
  docker compose logs
}

function config {
  docker compose config
}

#shellcheck disable=SC2120
function up {
  obtian_settings
  read_cache
  docker network inspect demi >/dev/null 2>&1 || {
    echo "Creating Docker network 'demi'"
    docker network create demi
  }
  docker compose up $1
}

#shellcheck disable=SC2120
function down {
  docker compose down $1
}

function recompose {
  down
  up
}

function help {
  echo "usage: $0 <task> <args>"
  # declare -F | awk '{print $NF}' | sort | egrep -v "^_"
  compgen -A function | cat -n
  exit 0
}

"${@:-all}"
