import { useState, useRef } from 'react'

export default function useConnectivityList (
  onAddSlice = (id) => {},
  onSetValue = (id, opt, value) => {}
) {
  const defaultVals = {
    id: 0,
    configData: '',
    configName: '',
    rx: undefined,
    tx: undefined,
    host: undefined,
    cmd_port: undefined,
    data_port: undefined,
    enable: 1,
    locked: 0,
    upload: true
  }

  // use useRef, because then changes don't automatically result in full
  // re-render of the entire connectivity input-array
  const connectivity = useRef([defaultVals])
  const [connVisibility, setConnVisibility] = useState([true])

  const resetConnectivity = (newConn) => {
    connectivity.current = newConn
    const tmpVisibility = newConn.map(conn => true)
    setConnVisibility([...tmpVisibility])
  }

  const setValue = (id, opt, val) => {
    connectivity.current[id][opt] = val
    onSetValue(id, opt, val)
  }

  const addConnSlice = () => {
    if (connectivity.current.length > 0) {
      connectivity.current.push({
        ...connectivity.current[connectivity.current.length - 1],
        id: connectivity.current.length,
        hidden: false
      })
      const curVisibilityList = connVisibility
      curVisibilityList.push(true)
      setConnVisibility([...curVisibilityList])
      onAddSlice(connectivity.current.length)
    } else {
      connectivity.current = [defaultVals]
      setConnVisibility([true])
    }
  }

  const removeConnSlice = (id) => {
    const tmpVisibility = connVisibility
    tmpVisibility[id] = false
    setConnVisibility([...tmpVisibility])
  }

  return [
    connectivity.current,
    connVisibility,
    setValue,
    addConnSlice,
    removeConnSlice,
    resetConnectivity
  ]
}
