import { useState, useEffect } from 'react'
import { generalRequest } from '@itk-demo-sw/utility-functions'

/**
 * useDataList provides a general Interface to list-like data to be obtained
 * from the DAQ API backend. Only supports GET requests.
 *
 * Params:
 *  url: string - Full API call
 *  resultKey: string - Key of the result array in the API response payload
 *  onError: function - Callback for errors (in RCF7807 format)
 *  handleLog: function - Callback to handle messages from the API
 *
 * Returns:
 * [
 *  dataList: array - List as returned by the API
 *  updateDataList: function - Repeats the API call and updates the dataList
 * ]
 */

export default function useDataList (
  backendUrl,
  endpoint,
  resultKey,
  onError = (data) => {},
  handleLog = (data) => {},
  dbAvailable = true
) {
  const [dataList, setDataList] = useState([])

  const updateDataList = () => {
    if (dbAvailable) {
      generalRequest(
        backendUrl + endpoint
      ).then(
        data => {
          handleLog(data)
          setDataList(data.payload[resultKey])
        }
      ).catch(
        err => {
          onError(err)
        }
      )
    } else {
      setDataList([])
    }
  }

  useEffect(() => {
    updateDataList()
  }, [backendUrl, dbAvailable])

  return [dataList, updateDataList]
}
