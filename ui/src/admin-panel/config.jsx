import createConfig from 'react-runtime-config'

export const { useConfig, useAdminConfig } = createConfig({
  namespace: 'MY_APP_CONFIG',
  schema: {
    backend: {
      type: 'custom',
      title: 'Backend URL/ prefix',
      description: 'Backend URL/ prefix',
      default: process.env.REACT_APP_CONFIGDB_BACKEND_URL || '/api',
      parser: (value) => {
        if (typeof value === 'string') {
          return value
        } else {
          throw new Error('Invalid backend URL')
        }
      }
    },
    healthFrequency: {
      type: 'number',
      title: '/health call frequency [ms]',
      description: '/health call frequency [ms]',
      default: 1000
    },
    callHealth: {
      type: 'boolean',
      title: 'Call /health endpoint regularly',
      description: 'Call /health endpoint regularly',
      default: true
    }
  }
})
