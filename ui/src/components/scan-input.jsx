import React, { useEffect } from 'react'
import {
  Bullseye,
  Button,
  Grid,
  GridItem
} from '@patternfly/react-core'
import { generalRequest } from '@itk-demo-sw/utility-functions'
// Components
import {
  TypeaheadSelectInput
} from '@itk-demo-sw/components'
// Hooks
import useDataList from '../hooks/useDataList'
import {
  useTypeaheadSelectInput
} from '@itk-demo-sw/hooks'
// PropTypes
import PropTypes from 'prop-types'

const ScanInput = (props) => {
  const [runkeyList, updateRunkeyList] = useDataList(
    props.daqApiUrl, '/db/runkeys', 'runkeys', props.onError, props.handleLog
  )
  const [scanCfgList, updateScanCfgList] = useDataList(
    props.daqApiUrl, '/db/scan_configs', 'scanConfigs', props.onError, props.handleLog
  )
  const [
    selectedRunkey,
    isRunkeyOpen,
    onRunkeySelect,
    onRunkeyToggle,
    clearRunkeySelection
  ] = useTypeaheadSelectInput()
  const [
    selectedScanCfg,
    isScanCfgOpen,
    onScanCfgSelect,
    onScanCfgToggle,
    clearScanCfgSelection
  ] = useTypeaheadSelectInput()

  const startScan = (rk, scanCfg) => {
    const body = {
      runkey: rk,
      scan_config: scanCfg
    }
    generalRequest(
      `${props.daqApiUrl}/daq/run`,
      body
    ).then(
      data => {
        if ((data.status === 200) && (data.messages.length === 0)) {
          props.onSuccess('Successfully executed scan.')
        } else {
          props.handleLog(data)
        }
      }
    ).catch(
      err => props.onError(err)
    )
    props.onInfo('Submitted scan.', 3000)
    props.getHealth(props.daqApiUrl)
  }

  useEffect(() => {
    if (props.connectionEstablished) {
      updateScanCfgList()
      updateRunkeyList()
    }
  }, [props.connectionEstablished])

  const runkeySelect = (
    <GridItem span={3}>
      <TypeaheadSelectInput
        key={'rk select'}
        selectOptions={runkeyList}
        selected={selectedRunkey}
        isOpen={isRunkeyOpen}
        onSelect={onRunkeySelect}
        onToggle={onRunkeyToggle}
        clearSelection={clearRunkeySelection}
        placeholderText={'Select a runkey'}
        maxheight="450%"
      />
    </GridItem>
  )

  const scanCfgSelect = (
    <GridItem span={3}>
      <TypeaheadSelectInput
        key={'rk select'}
        selectOptions={scanCfgList}
        selected={selectedScanCfg}
        isOpen={isScanCfgOpen}
        onSelect={onScanCfgSelect}
        onToggle={onScanCfgToggle}
        clearSelection={clearScanCfgSelection}
        placeholderText={'Select a scan'}
        maxheight="450%"
      />
    </GridItem>
  )

  const runButton = (
    <GridItem span={3}>
      <Button
        onClick={() => startScan(selectedRunkey, selectedScanCfg)}
      >
        {'Start Scan'}
      </Button>
    </GridItem>
  )

  return (
    <Bullseye>
      <Grid>
        {runkeySelect}
        {scanCfgSelect}
        {runButton}
      </Grid>
    </Bullseye>
  )
}

ScanInput.propTypes = {
  connectionEstablished: PropTypes.bool.isRequired,
  daqApiUrl: PropTypes.string.isRequired,
  getHealth: PropTypes.func.isRequired,
  handleLog: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
  onInfo: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired
}

export default ScanInput
