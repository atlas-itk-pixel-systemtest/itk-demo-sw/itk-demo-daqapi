import React, { useState, useMemo } from 'react'
import {
  Bullseye,
  Button,
  Checkbox,
  Flex,
  FlexItem,
  Grid,
  GridItem,
  Text,
  TextContent
} from '@patternfly/react-core'
import {
  MinusIcon,
  PlusIcon
} from '@patternfly/react-icons'
// Components
import {
  CheckedTextInput,
  ConfigSelection,
  IntegerInput
} from '@itk-demo-sw/components'
// Hooks
import {
  useCheckedTextInput,
  useConfigSelection,
  useIntegerInput
} from '@itk-demo-sw/hooks'
// PropTypes
import PropTypes from 'prop-types'

// const List = () => (<></>)

const ConnectivitySlice = (props) => {
  const [, handleRxChange] = useIntegerInput(
    props.curConn.rx, (val) => props.handleOptionChange('rx', val)
  )
  const [, handleTxChange] = useIntegerInput(
    props.curConn.tx, (val) => props.handleOptionChange('tx', val)
  )
  const [, handleCmdPortChange] = useIntegerInput(
    props.curConn.cmd_port, (val) => props.handleOptionChange('cmd_port', val)
  )
  const [, handleDataPortChange] = useIntegerInput(
    props.curConn.data_port, (val) => props.handleOptionChange('data_port', val)
  )
  const [, handleHostChange] = useCheckedTextInput(
    props.curConn.host, (val) => props.handleOptionChange('host', val)
  )
  const [enable, setEnable] = useState(props.curConn.enable)
  const [locked, setLocked] = useState(props.curConn.locked)
  const [
    ,
    ,
    ,
    handleConfigChange,
    handleUploadChange,
    isSelectOpen,
    onSelect,
    onToggle,
    clearSelection
  ] = useConfigSelection(
    props.curConn.upload,
    props.curConn.configData,
    props.curConn.configName,
    (data, name) => {
      props.handleOptionChange('configName', name)
      props.handleOptionChange('configData', data)
    },
    (upload) => {
      props.handleOptionChange('upload', upload)
    }
  )

  const configSelection = (
    <ConfigSelection
      type={'Frontend'}
      upload={props.curConn.upload}
      configName={props.curConn.configName}
      configData={props.curConn.configData}
      handleConfigChange={handleConfigChange}
      handleUploadChange={handleUploadChange}
      isSelectOpen={isSelectOpen}
      onSelect={onSelect}
      onToggle={onToggle}
      clearSelection={clearSelection}
      configList={props.frontendConfigList}
      selectDisabled={!props.dbAvailable}
      key={`fe config ${props.curConn.id}`}
    />
  )

  const txField = (
    <IntegerInput
      value={props.curConn.tx}
      name={'Tx'}
      handleChange={handleTxChange}
      key={`tx ${props.curConn.id}`}
    />
  )

  const rxField = (
    <IntegerInput
      value={props.curConn.rx}
      name={'Rx'}
      handleChange={handleRxChange}
      key={`rx ${props.curConn.id}`}
    />
  )

  const hostField = (
    <CheckedTextInput
      value={props.curConn.host}
      name={'Host'}
      handleChange={handleHostChange}
      optional
      key={`host ip ${props.curConn.id}`}
    />
  )

  const cmdField = (
    <IntegerInput
      value={props.curConn.cmd_port}
      name={'cmd port'}
      handleChange={handleCmdPortChange}
      optional
      key={`cmd port ${props.curConn.id}`}
    />
  )

  const dataField = (
    <IntegerInput
      value={props.curConn.data_port}
      name={'data port'}
      handleChange={handleDataPortChange}
      optional
      key={`data port ${props.curConn.id}`}
    />
  )

  const enableCheckbox = (
    <Checkbox
      id="enable-checkbox"
      label="Enable"
      isChecked={props.curConn.enable}
      onChange={() => {
        const curEnable = enable
        props.handleOptionChange('enable', !curEnable * 1)
        setEnable(!curEnable * 1)
      }}
      key={`enable check ${props.curConn.id}`}
    />
  )

  const lockedCheckbox = (
    <Checkbox
      id="locked-checkbox"
      label="Locked"
      isChecked={props.curConn.locked}
      onChange={() => {
        const curLocked = locked
        props.handleOptionChange('locked', !curLocked * 1)
        setLocked(!curLocked * 1)
      }}
      key={`locked check ${props.curConn.id}`}
    />
  )

  const removeButton = (
    <Button
      variant='plain'
      onClick={() => props.setHidden()}
      key={`rm button ${props.curConn.id}`}
    >
      <MinusIcon
        key={`rm icon ${props.curConn.id}`}
        style={{ color: '#C9190B' }}
      />
    </Button>
  )

  return (
    <Grid key={`fe grid ${props.curConn.id}`}>
      <GridItem span={1} key={`fe ${props.curConn.id} number item`}>
        <Bullseye key={`fe ${props.curConn.id} number bullseye`}>
          <TextContent key={`fe ${props.curConn.id} number text content`}>
            <Text
             component="h2"
             key={`fe ${props.curConn.id} number text`}
            >
              {`${props.number + 1}.`}
            </Text>
          </TextContent>
        </Bullseye>
      </GridItem>

      <GridItem span={10} key={`fe ${props.curConn.id} main item`}>
        <Flex key={`fe ${props.curConn.id} main flex`}>
          <FlexItem key={`fe ${props.curConn.id} main conf sel item`}>
            {configSelection}
          </FlexItem>
          <FlexItem key={`fe ${props.curConn.id} main rx item`}>
            {rxField}
          </FlexItem>
          <FlexItem key={`fe ${props.curConn.id} main tx item`}>
            {txField}
          </FlexItem>
          <FlexItem key={`fe ${props.curConn.id} main host item`}>
            {hostField}
          </FlexItem>
          <FlexItem key={`fe ${props.curConn.id} main cmd item`}>
            {cmdField}
          </FlexItem>
          <FlexItem key={`fe ${props.curConn.id} main data item`}>
            {dataField}
          </FlexItem>
          <FlexItem key={`fe ${props.curConn.id} main enable item`}>
            {enableCheckbox}
          </FlexItem>
          <FlexItem key={`fe ${props.curConn.id} main locked item`}>
            {lockedCheckbox}
          </FlexItem>
        </Flex>
      </GridItem>
      <GridItem span={1} key={`fe ${props.curConn.id} rm item`}>
        <Bullseye key={`fe ${props.curConn.id} rm bullseye`}>
          {removeButton}
        </Bullseye>
      </GridItem>
    </Grid>
  )
}

ConnectivitySlice.propTypes = {
  curConn: PropTypes.object.isRequired,
  dbAvailable: PropTypes.bool.isRequired,
  frontendConfigList: PropTypes.array.isRequired,
  handleOptionChange: PropTypes.func.isRequired,
  number: PropTypes.number.isRequired,
  setHidden: PropTypes.func.isRequired
}

const ConnectivityList = (props) => {
  const connSlices = useMemo(() => {
    return props.connectivity.filter(
      (conn, i) => (props.connVisibility[i])
    ).map((conn, i) => {
      return (
        <GridItem span={12} key={`conn_griditem_${conn.id}`}>
          <ConnectivitySlice
            key={`conn_slice_${conn.id}`}
            curConn={conn}
            number={i}
            frontendConfigList={props.frontendConfigList}
            dbAvailable={props.dbAvailable}
            handleOptionChange={
              (op, val) => props.setValue(conn.id, op, val)
            }
            setHidden={() => props.removeConnSlice(conn.id)}
          />
        </GridItem>
      )
    })
  }, [props.connVisibility, props.dbAvailable, props.frontendConfigList])

  const addConnButton = (
    <GridItem span={12} key={'add conn item'}>
      <Grid key={'add conn grid'}>
        <GridItem offset={11} span={1} key={'add conn subitem'}>
          <Bullseye key={'add conn bullseye'}>
            <Button
              key={'add conn button'}
              variant="plain"
              onClick={() => props.addConnSlice()}
            >
              <PlusIcon
                style={{ color: '#3E8635' }}
                key={'add conn icon'}
              />
            </Button>
          </Bullseye>
        </GridItem>
      </Grid>
    </GridItem>
  )

  return (
    <>
      {connSlices}
      {addConnButton}
    </>
  )
}

ConnectivityList.propTypes = {
  addConnSlice: PropTypes.func.isRequired,
  connectivity: PropTypes.array.isRequired,
  connVisibility: PropTypes.array.isRequired,
  dbAvailable: PropTypes.bool.isRequired,
  frontendConfigList: PropTypes.array.isRequired,
  removeConnSlice: PropTypes.func.isRequired,
  setValue: PropTypes.func.isRequired
}

export default ConnectivityList
