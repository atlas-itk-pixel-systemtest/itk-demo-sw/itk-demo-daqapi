import React from 'react'
import {
  Flex,
  FlexItem,
  PageSection,
  Text,
  TextContent
} from '@patternfly/react-core'
// PropTypes
import PropTypes from 'prop-types'

const PanelTitle = (props) => {
  const daqNameTitle = (
    <Text
      key={`${props.title} title daq`}
      component="h1"
      style={{
        color: 'RoyalBlue',
        fontSize: '30px',
        fontStyle: 'italic',
        display: 'inline-block',
        whiteSpace: 'pre-wrap'
      }}
    >
      {' - ' + props.daqName}
    </Text>
  )

  const stateText = (
    <Text
      key={`${props.title} state daq`}
      component="h1"
      style={{
        color: 'RoyalBlue',
        fontSize: '25px',
        fontFamily: 'Courier New',
        display: 'inline-block',
        whiteSpace: 'pre-wrap'
      }}
    >
      {'   ' + props.state}
    </Text>
  )

  return (
    <PageSection
      style={{
        background: 'linear-gradient(AliceBlue,white)',
        backgroundColor: 'White'
      }}
    >
      <Flex key={`${props.title} title flex`}>
        <FlexItem key={`${props.title} title text flex item`}>
          <TextContent key={`${props.title} textcon`}>
            <Text
              key={`${props.title} title text`}
              component="h1"
              style={{
                color: 'RoyalBlue',
                fontSize: '30px',
                display: 'inline-block',
                whiteSpace: 'pre-wrap'
              }}
            >
              {props.title}
            </Text>
            {props.daqName ? daqNameTitle : null}
            {props.state ? stateText : null}
            <Text
              key={`${props.title} subtitle text`}
              component="p"
              style={{
                color: 'RoyalBlue',
                fontSize: '20px',
                whiteSpace: 'pre-wrap'
              }}
            >
              {props.subtext}
            </Text>
          </TextContent>
        </FlexItem>
      </Flex>
    </PageSection>
  )
}

PanelTitle.propTypes = {
  title: PropTypes.string.isRequired,
  subtext: PropTypes.string.isRequired,
  daqName: PropTypes.string,
  state: PropTypes.string
}

export default PanelTitle
