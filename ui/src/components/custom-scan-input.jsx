import React, { useState, useEffect } from 'react'
import {
  Bullseye,
  Button,
  Flex,
  FlexItem,
  Grid,
  GridItem,
  Modal,
  ModalVariant,
  Text,
  TextContent,
  Tooltip
} from '@patternfly/react-core'
import { generalRequest } from '@itk-demo-sw/utility-functions'
// Components
import ConnectivityList from './connectivity-list'
import {
  CheckedTextInput,
  ConfigSelection,
  IntegerInput,
  TypeaheadSelectInput
} from '@itk-demo-sw/components'
// Hooks
import useDataList from '../hooks/useDataList'
import useConnectivityList from '../hooks/useConnectivityList'
import {
  useCheckedTextInput,
  useConfigSelection,
  useIntegerInput,
  useTypeaheadSelectInput
} from '@itk-demo-sw/hooks'
// PropTypes
import PropTypes from 'prop-types'

const CustomScanInput = (props) => {
  const [ctrlConfigList, updateCtrlConfigList] = useDataList(
    props.daqApiUrl, '/db/get_felix_configs',
    'felixConfigs', props.onError, props.handleLog, props.dbAvailable
  )
  const [scanConfigList, updateScanConfigList] = useDataList(
    props.daqApiUrl, '/db/get_scan_configs',
    'scanConfigs', props.onError, props.handleLog, props.dbAvailable
  )
  const [
    uploadCtrlConfig,
    ctrlConfigData,
    ctrlConfigName,
    handleCtrlConfigChange,
    handleCtrlUploadChange,
    isCtrlSelectOpen,
    onCtrlSelect,
    onCtrlToggle,
    clearCtrlSelection,
    resetCtrlData
  ] = useConfigSelection()
  const [
    uploadScanConfig,
    scanConfigData,
    scanConfigName,
    handleScanConfigChange,
    handleScanUploadChange,
    isScanSelectOpen,
    onScanSelect,
    onScanToggle,
    clearScanSelection
  ] = useConfigSelection()

  // Optional Options
  const [chipType, handleChipTypeChange] = useCheckedTextInput()
  const [targetCharge, setTargetCharge] = useIntegerInput()
  const [targetThresh, setTargetThresh] = useIntegerInput()
  const [targetTot, setTargetTot] = useIntegerInput()

  const [frontendConfigList, updateFrontendConfigList] = useDataList(
    props.daqApiUrl, '/db/get_frontend_configs',
    'frontendConfigs', props.onError, props.handleLog, props.dbAvailable
  )

  const [
    connectivity,
    connVisibility,
    setValue,
    addConnSlice,
    removeConnSlice,
    resetConnectivity
  ] = useConnectivityList(
    (id) => { setRunkeyId(-1) },
    (id, opt, val) => { setRunkeyId(-1) }
  )

  // Save runkey state variables
  const [showRunkeyModal, setShowRunkeyModal] = useState(false)
  const [runkeyTag, handleRunkeyTagChange] = useCheckedTextInput()

  // Load runkey state variables
  const [runkeyList, updateRunkeyList] = useDataList(
    props.daqApiUrl, '/db/get_runkeys',
    'runkeys', props.onError, props.handleLog, props.dbAvailable
  )
  const [loadingRunkey, setLoadingRunkey] = useState(false)
  const [showLoadRunkeyModal, setShowLoadRunkeyModal] = useState(false)
  const [
    selectedRunkey,
    isRunkeyOpen,
    onRunkeySelect,
    onRunkeyToggle,
    clearRunkeySelection
  ] = useTypeaheadSelectInput()
  const [runkeyId, setRunkeyId] = useState(-1)

  // Connections to the database

  const saveRunkey = (body) => {
    generalRequest(
      `${props.daqApiUrl}/db/save_runkey`,
      body
    ).then(
      data => {
        if ((data.status === 200) && (data.messages.length === 0)) {
          props.onSuccess(data.messages[0])
        } else {
          props.handleLog(data)
        }
        handleRunkeyTagChange(data.payload.runkeyTag)
        updateRunkeyList()
      }
    ).catch(
      err => {
        handleRunkeyTagChange(undefined)
        props.onError(err)
      }
    )
  }

  const loadRunkey = (runkey) => {
    generalRequest(
      `${props.daqApiUrl}/db/load_runkey`,
      { runkey: runkey }
    ).then(
      data => {
        props.handleLog(data)

        // Set controller config
        resetCtrlData(data.payload.runkey.ctrlConfigId.toString(), '', false)

        // Set fe connectivity
        const feConnList = []
        for (let i = 0; i < data.payload.runkey.feConfigs.length; i++) {
          feConnList.push({
            configName: data.payload.runkey.feConfigs[i].configId.toString(),
            id: i,
            configData: '',
            upload: false,
            hidden: false,
            ...data.payload.runkey.feConfigs[i]
          })
        }
        resetConnectivity([...feConnList])

        setLoadingRunkey(false)
        setShowLoadRunkeyModal(false)
        clearRunkeySelection()
        return data.payload.runkey.id
      }
    ).then(
      (id) => {
        // Buffer runkey until changes are made
        setRunkeyId(id)
      }
    ).catch(
      err => {
        setLoadingRunkey(false)
        setShowLoadRunkeyModal(false)
        clearRunkeySelection()
        props.onError(err)
      }
    )
  }

  const executeScan = (body) => {
    let endpoint = ''
    if ('runkey' in body) {
      endpoint = 'runkey'
    } else {
      endpoint = 'general'
    }
    generalRequest(
      `${props.daqApiUrl}/scan/${endpoint}`,
      body
    ).then(
      data => {
        if ((data.status === 200) && (data.messages.length === 0)) {
          props.onSuccess(data.messages[0])
        } else {
          props.handleLog(data)
        }
      }
    ).catch(
      err => props.onError(err)
    )
  }

  // Effects
  useEffect(() => {
    if (props.connectionEstablished && props.dbAvailable) {
      updateFrontendConfigList()
      updateCtrlConfigList()
      updateScanConfigList()
      updateRunkeyList()
    }
  }, [props.dbAvailable, props.connectionEstablished])

  useEffect(() => {
    setRunkeyId(-1)
  }, [ctrlConfigName, ctrlConfigData, uploadCtrlConfig])

  const getConnectivityOptions = () => {
    return {
      ctrlConfigData: ctrlConfigData,
      ctrlConfigName: ctrlConfigName,
      feConfigs: connectivity.map(
        (conn) => {
          return {
            configName: conn.configName,
            configData: conn.configData,
            rx: parseInt(conn.rx),
            tx: parseInt(conn.tx),
            cmd_port: conn.cmd_port !== undefined
              ? parseInt(conn.cmd_port)
              : 12350,
            data_port: conn.data_port !== undefined
              ? parseInt(conn.data_port)
              : 12360,
            host: conn.host !== undefined ? conn.host : '127.0.0.1',
            enable: conn.enable !== undefined ? conn.enable : 1,
            locked: conn.locked !== undefined ? conn.locked : 0
          }
        }
      ).filter(
        (conn, i) => connVisibility[i]
      )
    }
  }

  // UI Elements
  const scanConfigUpload = (
    <Flex key={'scan config flex'}>
      <FlexItem key={'scan config title item'}>
        <TextContent
          style={{ width: '250px' }}
          key={'scan config title textcon'}
        >
          <Text
            component="h3"
            key={'scan config title text'}
          >
            {'Scan Config'}
          </Text>
        </TextContent>
      </FlexItem>
      <FlexItem key={'scan config main item'}>
        <ConfigSelection
          key={'scan config main confsel'}
          type={'Scan'}
          upload={uploadScanConfig}
          configName={scanConfigName}
          configData={scanConfigData}
          handleConfigChange={handleScanConfigChange}
          handleUploadChange={handleScanUploadChange}
          isSelectOpen={isScanSelectOpen}
          onSelect={onScanSelect}
          onToggle={onScanToggle}
          clearSelection={clearScanSelection}
          configList={scanConfigList}
          selectDisabled={!props.dbAvailable}
        />
      </FlexItem>
    </Flex>
  )

  const ctrlConfigUpload = (
    <Flex key={'ctrl config flex'}>
      <FlexItem key={'ctrl config title item'}>
        <TextContent
          style={{ width: '250px' }}
          key={'ctrl config title textcon'}
        >
          <Text
            component="h3"
            key={'ctrl config title text'}
          >
            {'FELIX Config'}
          </Text>
        </TextContent>
      </FlexItem>
      <FlexItem key={'ctrl config main item'}>
        <ConfigSelection
          key={'ctrl config main confsel'}
          type={'FELIX'}
          upload={uploadCtrlConfig}
          configName={ctrlConfigName}
          configData={ctrlConfigData}
          handleConfigChange={handleCtrlConfigChange}
          handleUploadChange={handleCtrlUploadChange}
          isSelectOpen={isCtrlSelectOpen}
          onSelect={onCtrlSelect}
          onToggle={onCtrlToggle}
          clearSelection={clearCtrlSelection}
          configList={ctrlConfigList}
          selectDisabled={!props.dbAvailable}
        />
      </FlexItem>
    </Flex>
  )

  const connectivitySection = (
    <>
      <GridItem span={12} key={'conn title item'}>
        <TextContent key={'conn title textcon'}>
          <Text
            component="h3"
            key={'conn title text'}
          >
            {'Frontend Connectivity'}
          </Text>
        </TextContent>
      </GridItem>
      <ConnectivityList
        addConnSlice={addConnSlice}
        connectivity={connectivity}
        connVisibility={connVisibility}
        dbAvailable={props.dbAvailable}
        frontendConfigList={frontendConfigList}
        removeConnSlice={removeConnSlice}
        setValue={setValue}
      />
    </>
  )

  const chipTypeInput = (
    <GridItem offset={1} span={11} key={'chip type item'}>
      <Flex key={'chip type flex'}>
        <FlexItem key={'chip type title item'}>
          <TextContent
            style={{ width: '200px' }}
            key={'chip type title textcon'}
          >
            <Text
              key={'chip type title text'}
              component="h3"
            >
              {'Chip Type'}
            </Text>
          </TextContent>
        </FlexItem>
        <FlexItem key={'chip type main item'}>
          <CheckedTextInput
            key={'chip type main input'}
            name="Chip Type"
            value={chipType}
            handleChange={handleChipTypeChange}
            optional
            width='200px'
          />
        </FlexItem>
      </Flex>
    </GridItem>
  )

  const targetValuesInput = (
    <GridItem offset={1} span={11} key={'target vals item'}>
      <Flex key={'target vals flex'}>
        <FlexItem key={'target vals title item'}>
          <TextContent
            key={'target vals title textcon'}
            style={{ width: '200px' }}
          >
            <Text
              key={'target vals title text'}
              component="h3"
            >
              {'Target Values'}
            </Text>
          </TextContent>
        </FlexItem>
        <FlexItem key={'target vals charge item'}>
          <IntegerInput
            key={'target vals charge input'}
            value={targetCharge}
            handleChange={setTargetCharge}
            name={'Charge'}
            optional
          />
        </FlexItem>
        <FlexItem key={'target vals thresh item'}>
          <IntegerInput
             key={'target vals thresh input'}
            value={targetThresh}
            handleChange={setTargetThresh}
            name={'Threshold'}
            optional
          />
        </FlexItem>
        <FlexItem key={'target vals tot item'}>
          <IntegerInput
            key={'target vals tot input'}
            value={targetTot}
            handleChange={setTargetTot}
            name={'ToT'}
            optional
          />
        </FlexItem>
      </Flex>
    </GridItem>
  )

  const optionalOptions = (
    <>
      <GridItem span={12} key={'optional opts item'}>
        <TextContent key={'optional opts textcon'}>
          <Text
            key={'optional opts text'}
            component="h3"
          >
            {'Optional parameters'}
          </Text>
        </TextContent>
      </GridItem>
      {chipTypeInput}
      {targetValuesInput}
    </>
  )

  const saveRunkeyButton = (
    <>
      <Tooltip
        key={'save rk tooltip'}
        content={
          !props.connectionEstablished
            ? 'DAQ backend is not available'
            : 'Config DB is not available'
          }
        trigger={!props.dbAvailable ? 'mouseenterfocus' : 'manual'}
        isVisible={false}
      >
        <div key={'save rk wrapper'}>
          <Button
            key={'save rk button'}
            variant={'secondary'}
            onClick={() => setShowRunkeyModal(true)}
            isDisabled={!props.dbAvailable || !props.connectionEstablished}
          >
            Save Runkey
          </Button>
        </div>
      </Tooltip>
      <Modal
        key={'save rk modal'}
        title={'Add a runkey tag'}
        variant={ModalVariant.small}
        isOpen={showRunkeyModal}
        onClose={() => setShowRunkeyModal(false)}
        actions={[
          <Button key="confirm save rk" variant="primary" onClick={() => {
            const body = {
              tag: runkeyTag,
              ...getConnectivityOptions()
            }
            saveRunkey(body)
            setShowRunkeyModal(false)
          }}>
            Confirm
          </Button>,
          <Button
            key="cancel save rk"
            variant="link"
            onClick={() => setShowRunkeyModal(false)}
          >
            Cancel
          </Button>
        ]}
      >
        <TextContent key={'save rk textcon'}>
          <Text component="p" key={'save rk text'}>
            {'Leave empty to save runkey without a tag.'}
          </Text>
        </TextContent>
        <CheckedTextInput
          key={'save rk name input'}
          name={'Runkey tag'}
          value={runkeyTag}
          handleChange={handleRunkeyTagChange}
          optional
          width='80%'
        />
      </Modal>
    </>
  )

  const executeScanButton = (
    <Tooltip
      key={'ex scan tooltip'}
      content='DAQ backend is not available'
      trigger={!props.connectionEstablished ? 'mouseenterfocus' : 'manual'}
      isVisible={false}
    >
      <div key={'ex scan wrapper'}>
        <Button
          key={'ex scan button'}
          variant="primary"
          isDisabled={!props.connectionEstablished}
          onClick={() => {
            let body = {}
            if (runkeyId > -1) {
              body = {
                scanConfigData: scanConfigData,
                scanConfigName: scanConfigName,
                runkey: runkeyId.toString()
              }
            } else {
              body = {
                scanConfigData: scanConfigData,
                scanConfigName: scanConfigName,
                ...getConnectivityOptions()
              }
            }
            if (chipType !== '') body.chipType = chipType
            if (targetCharge !== undefined) body.targetCharge = parseInt(targetCharge)
            if (targetThresh !== undefined) body.targetThresh = parseInt(targetThresh)
            if (targetTot !== undefined) body.targetTot = parseInt(targetTot)

            executeScan(body)
            props.onInfo('Submitted scan.', 3000)
            props.getHealth(props.daqApiUrl)
          }}
        >
          Start Scan
        </Button>
      </div>
    </Tooltip>
  )

  const loadRunkeyButton = (
    <>
      <Tooltip
        key={'load rk tooltip'}
        content={
        !props.connectionEstablished
          ? 'DAQ backend is not available'
          : 'Config DB is not available'
        }
        trigger={!props.dbAvailable ? 'mouseenterfocus' : 'manual'}
        isVisible={false}
      >
        <div key={'load rk wrapper'}>
          <Button
            key={'load rk button'}
            variant={'secondary'}
            onClick={() => setShowLoadRunkeyModal(true)}
            isDisabled={!props.dbAvailable || !props.connectionEstablished}
          >
            Load Runkey
          </Button>
        </div>
      </Tooltip>
      <Modal
        key={'load rk modal'}
        style={{ overflow: 'visible' }}
        title={'Select a runkey'}
        variant={ModalVariant.small}
        isOpen={showLoadRunkeyModal}
        onClose={() => setShowLoadRunkeyModal(false)}
        actions={[
          <Button
            key="confirm load rk"
            variant="primary"
            isLoading={loadingRunkey}
            onClick={() => {
              loadRunkey(selectedRunkey)
              setLoadingRunkey(true)
            }}
          >
            Load
          </Button>,
          <Button
            key="cancel load rk"
            variant="link"
            onClick={() => setShowLoadRunkeyModal(false)}
          >
            Cancel
          </Button>
        ]}
      >
        <TypeaheadSelectInput
          key={'load rk select'}
          selectOptions={runkeyList}
          selected={selectedRunkey}
          isOpen={isRunkeyOpen}
          onSelect={onRunkeySelect}
          onToggle={onRunkeyToggle}
          clearSelection={clearRunkeySelection}
          placeholderText={'Select a runkey'}
          maxheight="450%"
          isDisabled={loadingRunkey}
        />
      </Modal>
    </>
  )

  const actionButtons = (
    <GridItem span={12} key={'action buttons item'}>
      <Bullseye key={'action buttons bullseye'}>
        <Flex key={'action buttons flex'}>
          <FlexItem key={'action buttons ex scan item'}>
            {executeScanButton}
          </FlexItem>
          <FlexItem key={'action buttons save rk item'}>
            {saveRunkeyButton}
          </FlexItem>
          <FlexItem key={'action buttons load rk item'}>
            {loadRunkeyButton}
          </FlexItem>
        </Flex>
      </Bullseye>
    </GridItem>
  )

  return (
    <Grid hasGutter key={'main grid'}>
      {scanConfigUpload}
      {ctrlConfigUpload}
      {connectivitySection}
      {optionalOptions}
      {actionButtons}
    </Grid>
  )
}

CustomScanInput.propTypes = {
  connectionEstablished: PropTypes.bool.isRequired,
  daqApiUrl: PropTypes.string.isRequired,
  dbAvailable: PropTypes.bool.isRequired,
  getHealth: PropTypes.func.isRequired,
  handleLog: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
  onInfo: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired
}

export default CustomScanInput
