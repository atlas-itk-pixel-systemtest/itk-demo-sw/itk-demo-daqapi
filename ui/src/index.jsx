import React from 'react'
import ReactDOM from 'react-dom/client'
import '@patternfly/react-core/dist/styles/base.css'
import './style.css'
import DaqDashboard from './screens/dashboard'
import PanelBadge from './admin-panel/panel-badge'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  <>
    <DaqDashboard
      key="main-content"
    />
    {process.env.REACT_APP_DEPLOYMENT ? null : (<PanelBadge/>)}
  </>
)
