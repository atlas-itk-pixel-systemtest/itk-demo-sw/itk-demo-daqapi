import React, { useState, useEffect } from 'react'
import { useConfig } from '../admin-panel/config'
import {
  Alert,
  Bullseye,
  Button,
  Modal,
  Page,
  PageNavigation,
  PageSection,
  PageSectionVariants,
  Spinner
} from '@patternfly/react-core'
import { generalRequest } from '@itk-demo-sw/utility-functions'
// Screens
import ScanPanel from './scan-panel'
import InitializationPanel from './initialization-panel'
// Components
import {
  LoggingViewer,
  Navbar,
  Notifications
} from '@itk-demo-sw/components'
// Hooks
import {
  useConfigureValue,
  useLoggingViewer,
  useNavbar,
  useNotifications
} from '@itk-demo-sw/hooks'

const DaqDashboard = (props) => {
  const [showLog, setShowLog] = useState(false)
  const [daq, setDaq] = useState('')
  const [state, setState] = useState('')
  const [connectionEstablished, setConnectionEstablished] = useState(false)
  const [logContent, addLog] = useLoggingViewer()
  const [backendAvailable, setBackendAvailable] = useState(false)
  const [initialLoad, setInitialLoad] = useState(true)
  const [intervalId, setIntervalId] = useState(0)
  const { getConfig, setConfig } = useConfig()
  const daqApiUrl = useConfigureValue('/config', 'urlKey', getConfig('backend'), 'backendUrl')
  useEffect(() => {
    setConfig('backend', daqApiUrl)
  }, [daqApiUrl])

  const healthFrequency = getConfig('healthFrequency')
  const callHealth = getConfig('callHealth')

  const disableAll = () => {
    setConnectionEstablished(false)
    setState('')
    setDaq('')
  }

  const getHealth = (daqApiUrl) => {
    generalRequest(`${daqApiUrl}/daq/info`).then(
      data => {
        handleLog(data)
        setState(data.payload.state)
        setDaq(data.payload.daqInfo.name)

        setConnectionEstablished(true)
        setBackendAvailable(true)
        setInitialLoad(false)
      }
    ).catch(
      (err) => {
        console.log(err)
        disableAll()
        setBackendAvailable(false)
      }
    )
  }

  useEffect(() => {
    clearInterval(intervalId)
    getHealth(daqApiUrl)
    setIntervalId(setInterval(() => { if (callHealth) getHealth(daqApiUrl) }, healthFrequency))
  }, [callHealth, healthFrequency, daqApiUrl])

  const [
    alerts,
    onError,
    onWarn,
    onInfo,
    onSuccess,
    deleteAlert
  ] = useNotifications(addLog)

  const handleLog = response => {
    if ((Object.keys(response).includes('status')) &&
      (Object.keys(response).includes('messages'))) {
      if (response.messages.length > 0) {
        for (const m of response.messages) {
          if (response.status === 200) onInfo(m, 6000)
          if (response.status === 299) onWarn(m, 6000)
        }
      }
    }
  }

  const noConnectionAlert = (
    connectionEstablished
      ? null
      : (
        <PageSection
          key={'no connection alert sec'}
          variant={PageSectionVariants.light}
          style={{ padding: '0px' }}
        >
          <Alert
            key={'no connection alert alert'}
            variant="danger"
            isInline
            title="DAQ API is not responding!"
          >
            {'Please check if the API backend is running. ' +
            `Currently trying to connect to "${
              daqApiUrl !== ''
                ? daqApiUrl
                : location.host
            }".`}
          </Alert>
        </PageSection>
        )
  )

  const loadingPage = (
    <PageSection
      key={'loading page section'}
      variant={PageSectionVariants.light}
      stickyOnBreakpoint='top'
      isFilled
    >
      <Bullseye key={'loading bullseye'}>
        <Spinner
          isSVG
          key={'loading spinner'}
          diameter="80px"
        />
      </Bullseye>
    </PageSection>
  )

  const initPanel = (
    <Modal
      key={'init section modal'}
      width={'50%'}
      showClose={false}
      isOpen={!((state.includes('Running') || state.includes('Idle') || state.includes('Done')) && state !== '')}
      aria-label='My Modal'
    >
      <InitializationPanel
        key={'init panel'}
        daqApiUrl={daqApiUrl}
        onError={onError}
        handleLog={handleLog}
        callHealth={getHealth}
        backendAvailable={backendAvailable}
      />
    </Modal>
  )

  const panels = [
    {
      title: 'Scan Panel',
      content: (
        <ScanPanel
          daqApiUrl={daqApiUrl}
          state={state}
          onError={onError}
          onInfo={onInfo}
          onSuccess={onSuccess}
          handleLog={handleLog}
          getHealth={getHealth}
          daq={daq}
          connectionEstablished={connectionEstablished}
        />
      )
    }
  ]

  const [activeItem, itemNames, changePanel] = useNavbar(
    panels.map((p) => p.title)
  )

  const notifications = (
    <Notifications
      key={'notifications'}
      alerts={alerts}
      deleteAlert={deleteAlert}
    />
  )

  const content = (
    <>
      <PageSection
        key={'navbar section'}
        padding={{ default: 'noPadding' }}
      >
        <PageNavigation key={'page nav'}>
          <Navbar
            key={'navbar'}
            activeItem={activeItem}
            changePanel={changePanel}
            itemNames={itemNames}
          />
        </PageNavigation>
      </PageSection>
      <PageSection
        key={'main content section'}
        isFilled
        hasOverflowScroll
        padding={{ default: 'noPadding' }}
      >
        {panels[activeItem].content}
      </PageSection>
    </>
  )

  const loggingSection = (
    <PageSection
      key={'logging section'}
      sticky='bottom'
      padding={{ default: 'noPadding' }}
      variant={PageSectionVariants.darker}
      isFilled={false}
      style={{
        backgroundColor: 'RoyalBlue'
      }}
    >
      <Button
        key={'toggle logging button'}
        isBlock
        variant="plain"
        onClick={(event) => setShowLog((prevShowLog) => !prevShowLog)}
      >
        Toggle Logging
      </Button>
      {showLog
        ? (
          <LoggingViewer content = {logContent} key={'logging view'}/>
          )
        : null
      }
    </PageSection>
  )

  return (
    <Page className="myPageClass">
      {backendAvailable || initialLoad ? null : noConnectionAlert}
      {notifications}
      {initialLoad ? loadingPage : null}
      {initialLoad ? null : initPanel}
      {initialLoad ? null : content}
      {initialLoad ? null : loggingSection}
    </Page>
  )
}

export default DaqDashboard
