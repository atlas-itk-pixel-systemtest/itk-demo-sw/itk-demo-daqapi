import React, { useState } from 'react'
import {
  Bullseye,
  Button,
  Grid,
  GridItem,
  Page,
  PageSection,
  PageSectionVariants
} from '@patternfly/react-core'
import { generalRequest } from '@itk-demo-sw/utility-functions'
// Components
import PanelTitle from '../components/panel-title'
import {
  TypeaheadSelectInput
} from '@itk-demo-sw/components'
// Hooks
import useDataList from '../hooks/useDataList'
import {
  useTypeaheadSelectInput
} from '@itk-demo-sw/hooks'
// PropTypes
import PropTypes from 'prop-types'

const InitializationPanel = (props) => {
  const [daq, setDaq] = useState('')
  const [buttonLoading, setButtonLoading] = useState(false)

  const [daqList] = useDataList(
    props.daqApiUrl,
    '/daq/available_daqs',
    'daqs',
    props.onError,
    props.handleLog,
    props.backendAvailable
  )

  const [
    selectedDaq,
    isDaqSelOpen,
    onDaqSelect,
    onDaqSelToggle,
    clearDaqSelection
  ] = useTypeaheadSelectInput(null, setDaq)

  const title = (
    <PanelTitle
      title={'Initialize the DAQ API'}
      subtext={
        'Please select the DAQ backend that you want to use.'
      }
    />
  )

  const daqSelect = (
    <TypeaheadSelectInput
      key={'init panel daq sel input'}
      selected={selectedDaq}
      selectOptions={daqList}
      isOpen={isDaqSelOpen}
      clearSelection={clearDaqSelection}
      onSelect={onDaqSelect}
      onToggle={onDaqSelToggle}
      placeholderText={'Select DAQ backend'}
      maxheight="450%"
    />
  )

  const initButton = (
    <Bullseye key={'init button bullseye'}>
      <Button
        key={'init button button'}
        variant='primary'
        isLoading={buttonLoading}
        onClick={() => {
          setButtonLoading(true)
          const body = {
            daq_system: daq
          }
          generalRequest(`${props.daqApiUrl}/daq/init`, body).then(
            data => {
              props.callHealth()
              props.handleLog(data)
              setButtonLoading(false)
            }
          ).catch(
            err => {
              setButtonLoading(false)
              props.onError(err)
            }
          )
        }}
      >
        Initialize
      </Button>
    </Bullseye>
  )

  const initPanel = (
    <PageSection
      isFilled
      variant={PageSectionVariants.light}
      stickyOnBreakpoint ='top'
    >
      <Bullseye key={'init panel bullseye'}>
        <Grid key={'init panel grid'} hasGutter>
          <GridItem span={6} key={'init panel grid item daq sel'}>
            {daqSelect}
          </GridItem>
          <GridItem span={6} key={'init panel grid item init button'}>
            {initButton}
          </GridItem>
        </Grid>
      </Bullseye>
    </PageSection>
  )

  return (
    <Page style={{ height: '100%' }}>
      {title}
      {initPanel}
    </Page>

  )
}

InitializationPanel.propTypes = {
  backendAvailable: PropTypes.bool.isRequired,
  callHealth: PropTypes.func.isRequired,
  daqApiUrl: PropTypes.string.isRequired,
  handleLog: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired
}

export default InitializationPanel
