import React from 'react'
import {
  Page,
  PageSection,
  PageSectionVariants
} from '@patternfly/react-core'
// Components
import PanelTitle from '../components/panel-title'
// import CustomScanInput from '../components/custom-scan-input'
import ScanInput from '../components/scan-input'
// PropTypes
import PropTypes from 'prop-types'

const ScanPanel = (props) => {
  const title = (
    <PanelTitle
      title={'Scan Control'}
      subtext={
        'Execute scans either using manually uploaded configuration ' +
        'files, or using configurations stored in the ConfigDB.'
      }
      daqName={props.daq}
      state={props.state ? props.state : ''}
    />
  )

  const content = (
    <PageSection
      key={'scan panel main section'}
      variant={PageSectionVariants.light}
      sticky='top'
      isFilled
    >
      {/* <CustomScanInput
        key={'scan panel main custom scan input'}
        daqApiUrl={props.daqApiUrl}
        onError={props.onError}
        onInfo={props.onInfo}
        onSuccess={props.onSuccess}
        handleLog={props.handleLog}
        daqName={props.daq}
        dbAvailable={props.dbAvailable}
        connectionEstablished={props.connectionEstablished}
        getHealth={props.getHealth}
      /> */}
      <ScanInput
        key={'scan panel main custom scan input'}
        daqApiUrl={props.daqApiUrl}
        onError={props.onError}
        onInfo={props.onInfo}
        onSuccess={props.onSuccess}
        handleLog={props.handleLog}
        connectionEstablished={props.connectionEstablished}
        getHealth={props.getHealth}
      />
    </PageSection>
  )

  return (
    <Page key={'scan panel page'}>
      {title}
      {content}
    </Page>
  )
}

ScanPanel.propTypes = {
  connectionEstablished: PropTypes.bool.isRequired,
  daq: PropTypes.string.isRequired,
  daqApiUrl: PropTypes.string.isRequired,
  getHealth: PropTypes.func.isRequired,
  handleLog: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
  onInfo: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  state: PropTypes.string
}

export default ScanPanel
