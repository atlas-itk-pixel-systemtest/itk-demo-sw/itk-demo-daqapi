#!/usr/bin/env bash

for folder in stacks/*; do

    if [[ $stack != "daqapi" ]]; then
        docker compose -f $folder/compose.yaml up -d
    fi
    
done
    