#!/usr/bin/env bash

make_config(){
    compose_file=$1
    config_file=$2

    stack=false
    host=false
    puid=false
    pgid=false
    ptz=false
    docker_dir=false
    runkey_dir=false

    # Determine what filed need to be added in config scripts
    if grep -q STACK $compose_file; then
        #echo "STACK found in $compose_file"
        stack=true
    fi

    if grep -q HOST $compose_file; then
        host=true
    fi

    if grep -q PUID $compose_file; then
        puid=true
    fi

    if grep -q PGID $compose_file; then
        pgid=true
    fi

    if grep -q PTZ $compose_file; then
        ptz=true
    fi

    if grep -q DOCKER_DIR $compose_file; then
        docker_dir=true
    fi

    if grep -q RUNKEY_DIR $compose_file; then
        runkey_dir=true
    fi


    # First part of config script: get env variable
    echo "#!/usr/bin/env bash" > $config_file
    echo "" >> $config_file

    [[ $stack == true ]] && echo "STACK=\${STACK-default}" >> $config_file
    [[ $host == true ]] && echo "HOST=\$(hostname)" >> $config_file
    [[ $puid == true ]] && echo "PUID=\$(id -u)" >> $config_file
    [[ $pgid == true ]] && echo "PGID=\$(id -g)" >> $config_file
    [[ $ptz == true ]] && echo "PTZ=\$(cat /etc/timezone)" >> $config_file
    [[ $docker_dir == true ]] && echo "DOCKER_DIR=\${DOCKER_DIR-\"\$(cd \"\$(dirname \"\$1\")\"; cd ../.. ; pwd -P)/\$(basename \"\$1\")\"docker}" >> $config_file
    [[ $runkey_dir == true ]] && echo "RUNKEY_DIR=\${RUNKEY_DIR-\"\$(cd \"\$(dirname \"\$1\")\"; cd ../.. ; pwd -P)/\$(basename \"\$1\")\"configdb}" >> $config_file

    # Second part of config script: commands not make .env file
    echo "" >> $config_file
    echo "cat <<-EOF >.env" >> $config_file
    [[ $stack == true ]] && echo "STACK=\${STACK}" >> $config_file
    [[ $host == true ]] && echo "HOST=\${HOST}" >> $config_file
    [[ $puid == true ]] && echo "PUID=\${PUID}" >> $config_file
    [[ $pgid == true ]] && echo "PGID=\${PGID}" >> $config_file
    [[ $ptz == true ]] && echo "PTZ=\${PTZ}" >> $config_file
    [[ $docker_dir == true ]] && echo "DOCKER_DIR=\${DOCKER_DIR}" >> $config_file
    [[ $runkey_dir == true ]] && echo "RUNKEY_DIR=\${RUNKEY_DIR}" >> $config_file
    echo "EOF" >> $config_file
    echo "" >> $config_file
    echo "cat .env" >> $config_file
    chmod u+x $config_file    
}

# Make the config file for each stack
for compose in stacks/*/compose.yaml; do 
    #stack=${folder##*/}
    conf_file="$(dirname "$compose")/config"
    echo "Making $conf_file from $compose"
    make_config $compose $conf_file
done

init_dir=$(pwd)

# Creat deminet network if needed
if docker network inspect deminet &> /dev/null; then 
    echo "Network deminet already existing"
else
    echo "Network deminet does not exist, making it"
    docker network create deminet
fi

# Configure and pull all stacks
for folder in stacks/*; do 
    stack=${folder##*/}
    echo "*** Configuring $stack in $folder ***"
    cd $folder
    ./config
    docker compose pull
    cd $init_dir
done
