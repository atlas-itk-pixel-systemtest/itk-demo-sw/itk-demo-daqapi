# How to use the example

## New instructions

Configure all stacks
```bash
./config
```
Launch all services (except `daqpi`)
```bash
./launch_services.sh
```
Wait a few seconds for the containers to start.
You can verify that everything was started sucessfully via the [dashboard](http://localhost). Make sure all dots are green or greyed out.

Create example runkeys in the configdb needed for emulator scan:
```bash
./setup
```

Start daq-api containers:
```bash
docker compose -f stacks/daqapi/compose.yaml up -d
```

#
---
#

## OLD INSTRUCTIONS

Start supporting services, like configdb, registry and dashboard with:
```bash
./config
docker compose -f services_compose.yaml up -d
```
Wait a few seconds for the containers to start.
You can verify that everything was started sucessfully via the [dashboard](http://localhost). Make sure all dots are green or greyed out.

Create example runkeys in the configdb needed for emulator scan:
```bash
./setup
```

Start daq-api containers:
```bash
docker compose up -d
```

Launch the [dashboard](http://localhost) in your browser and click on daq-api UI.
Select YARR and click Initialize. Select yarr_emulator as the runkey and digitalscan as scan config.

After clicking Start the state of the DAQ-API should switch to Running and than to Done after about a minute. The output of the scan is accessible via the docker logs. They will be propageted to the UI in a future update.

After running the scan its results will be saved in the yarr_emulator runkey. You can take a look at the results via the runkey-ui. Open it via the dashboard and search for the "yarr emualtor" runkey. Click on it to open, than navigate through the list to get to the scan_result and its content.
