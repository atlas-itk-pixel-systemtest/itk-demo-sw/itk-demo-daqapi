# ITk Demonstrator DAQ API

Before setting up the API, install pyenv, poetry and nodejs (ideally using the [demi bootstrapping tool](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/demi)). demi can also be used to install the DAQ API. Should you still wish to perform a manual installation, follow the instructions below.

For a general setup execute the following commands. Note that you need to authenticate to our private package registries in advance (using `demi python auth` and `demi nodejs auth`).

```
git clone https://gitlab.cern.ch/itk-demo-sw/itk-demo-daqapi.git
cd itk-demo-daqapi
poetry install
```

Specific configuration options are set in `src/DAQ_API_Config.py` and can be altered from their default values by setting corresponding environment variables. 

#### Config-DB backend
The DAQ API supports (and is best used) with a running [ITk Demo Config-DB](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-configdb) server in the backend. The DAQ API will then only take config-names as input and retrieve the payload directly from the database. Should you have a Config-DB server running, you need to set the `CONFIGDB_ADRESS` environment variable to the server address. This variable needs to be set at runtime. 



## openAPI implementation

The easiest way to test out the DAQ class's functionalities is via the readily implemented openAPI wrapper. The openAPI flask server is by default set to `http://localhost:5005` (see `src/DAQ_API_Config.py`). Start the server (from the root directory) via:
```
flask run
```
Before any further commands can be executed, the `init_daq` endpoint has to be called to initalize a DAQ of a certain type. The Swagger UI includes example calls for all API-endpoints, which initalize a [Dummy DAQ](#dummy-daq) and execute basic commands for that DAQ.  

Most sacn- and tune-endpoints require a running Config-DB server in the backend (to be defined in the `CONFIGDB_ADRESS` environment variable). The `/scan/custom` endpoint can be used, if no Config-DB  server is available. In this case, the config payload, as well as the full connectivity information need to be provided in the request. 

## GUI prototype

The current version of the DAQ API includes a GUI prototype, which allows to specify a DAQ of a certain type and execute basic scans. Currently, the scans available in the GUI require a running Config-DB server in the backend. Support for the `/scan/custom`endpoint is still to be added.

As a user or tester, you should transpile the GUI's javascript code. To do so, navigate to the project's root-directory and run
```
source compile.sh
```
This script will install nvm and npm, if the have not been installed yet, and transpile the javascript code. If you have done this, the GUI will be served by the flask server and available at `/`. You just need to run `flask run` again. 

Alternatively, especially if you are interested in development, you can run the GUI in debugging mode. The debugging mode automatically updates the GUI to changes in the javascript code, without the need for manual transpilation. The debugging mode requires two shells, one with the flask server running as a backend, and one with the GUI running. In the second shell, run 
```
cd ui/
npm run start
```
This will run a react-script, which starts a second server, which exclusively serves the GUI. It is set to port 3000 by default. If this port is occupied, the react-script should notice this and ask if the port should be changed. 
 

## DAQ class and supported DAQ systems 
The _DAQ class defines a set of basic methods that need to be overwritten for each implemented DAQ software. Scans can then be executed using the `ExecuteScan(scanoptions)` method. The `scanoptions` argument is a python dictionary, which includes all necessary scan information, including FE/ HW configs. The requirements for all currently implemented DAQ software packages are given in the following.

### YARR

For the YARR implementation, the [YARR python branch](https://gitlab.cern.ch/YARR/pyYARR) needs to be compiled on the system. 
Additionally, the `YARR_DIR` environment variable needs to be set to the directory, which includes the pyyarr library (likely `/<path>/<to>/pyYARR/build.<your_system>/python/`). 
The `scanoptions` dictionary needs to have the following structure:
<code>{
&nbsp; "CtrlCfg" : <i>parsed HW-controller config</i>,
&nbsp; "ScanCfg" : <i>parsed scan-controller config</i>,
&nbsp; "scantype" : <i>Name of scantype (ideally just scan-cfg name)</i>,
&nbsp; "chipType" : <i>type of chip</i>,
&nbsp; "FEconfigs" :  <i>List of FE-configurations</i>
&nbsp; [
&nbsp; &nbsp; {
&nbsp; &nbsp; &nbsp; "name" : <i>some name for the FE</i>,
&nbsp; &nbsp; &nbsp; "config" : <i>parsed FE config (not just path to config!)</i>,
&nbsp; &nbsp; &nbsp; "rx" : <i>FE rx channel</i>,
&nbsp; &nbsp; &nbsp; "tx" : <i>FE tx channel</i>,
&nbsp; &nbsp; &nbsp; "enable" : <i>FE enabled</i>,
&nbsp; &nbsp; &nbsp; "locked" : <i>FE locked</i>,
&nbsp; &nbsp; },
&nbsp; &nbsp; ...
&nbsp;  ]
}</code>
 
**Note:** Currently the `main` method of the `yarr.py` module (which includes the yarr implementation into the DAQ API) executes a digitalscan on RD53A chips using the RD53A emulator with the default config files provided by YARR. 

### Demonstrator DAQ

Requires `TDAQ_ENV` environment variable to be set to the Tdaq environment directory, which includes the Demonstrator DAQ implementation.

<i>TBD</i>

### Dummy DAQ

A mock DAQ, for which some basic scans and scan requirements are defined. All inherited DAQ functions check the input for required parameters and return output similar to that gerated by implementations of real DAQ systems. Useful for testing general functionalities (especially openAPI implementation).
